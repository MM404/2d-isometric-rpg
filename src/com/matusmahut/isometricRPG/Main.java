package com.matusmahut.isometricRPG;

import org.lwjgl.*;
import org.lwjgl.glfw.*;

import com.matusmahut.isometricRPG.engine.Game;
import com.matusmahut.isometricRPG.engine.input.CursorHandler;
import com.matusmahut.isometricRPG.engine.input.KeyHandler;
import com.matusmahut.isometricRPG.engine.input.MouseClickHandler;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryUtil.*;

import java.io.File;

public class Main {

	// Mus�me silno referencova� callback in�tancie pou��van� na input
	private GLFWErrorCallback errorCallback;
	private KeyHandler keyCallback;
	private CursorHandler cursorCallback;
	private MouseClickHandler mouseButtonCallback;

	private Game loop = new Game();

	// identifik�tor okna
	private long window;

	public void run() {
		System.out.println("Hello LWJGL " + Version.getVersion() + "!");

		try {
			init();
			loop.run(window, keyCallback, cursorCallback, mouseButtonCallback);
			// Zni�enie okna a callbackov
			glfwDestroyWindow(window);
		} finally {
			glfwTerminate();
		}
	}

	private void init() {
		System.setProperty("org.lwjgl.librarypath", new File("lib/natives").getAbsolutePath());
		// nastavenie error callbacku
		glfwSetErrorCallback(errorCallback = GLFWErrorCallback.createPrint(System.err));
		// Inicializovanie GLFW
		if (glfwInit() != GLFW_TRUE)
			throw new IllegalStateException("Unable to initialize GLFW");
		initWindow();

	}

	private void initWindow() {

		// konfigur�cia okna
		glfwDefaultWindowHints();	
		glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
		glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE);

		int WIDTH = 800;
		int HEIGHT = 600;
		
		window = glfwCreateWindow(WIDTH, HEIGHT, "Isometric Test", NULL, NULL);
		if (window == NULL)
			throw new RuntimeException("Failed to create the GLFW window");

		glfwSetKeyCallback(window, keyCallback = new KeyHandler());
		glfwSetCursorPosCallback(window, cursorCallback = new CursorHandler());
		glfwSetMouseButtonCallback(window, mouseButtonCallback = new MouseClickHandler());

		//z�skanie rozl�enia obrazovky
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		// vycentrovanie okna
		glfwSetWindowPos(window, (vidmode.width() - WIDTH) / 2, (vidmode.height() - HEIGHT) / 2);

		// nastavenie aktu�lneho OpenGL kontextu na toto okno!!
		glfwMakeContextCurrent(window);
		// zapnutie V-Sync
		glfwSwapInterval(1);
		// nastavenie vidite�nosti okna
		glfwShowWindow(window);

	}

	public KeyHandler getInputListener() {
		return keyCallback;
	}

	public static void main(String[] args) {
		new Main().run();
	}

}