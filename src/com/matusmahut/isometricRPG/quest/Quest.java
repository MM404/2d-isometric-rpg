package com.matusmahut.isometricRPG.quest;

import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.gameObject.units.Player;
import com.matusmahut.isometricRPG.map.Segment;

public class Quest implements EventHandler {

	private QuestState activeState;
	private String name;

	public Quest(Player player, QuestType type) {
		this.activeState = (QuestState) type.getState();
		this.activeState.setPlayer(player);
		this.name = type.getName();
	}

	public void setQuestStateDescription() {
		activeState.setDescription();
	}

	@Override
	public void handlePlayerPositionChange(Segment segment) {
		activeState.handlePlayerPositionChange(segment);
		if (activeState.isRequestingChange())
			activeState = activeState.getNextState();
	}

	@Override
	public void handleDialogChoice(ConversationType type, int i) {
		activeState.handleDialogChoice(type, i);
		if (activeState.isRequestingChange())
			activeState = activeState.getNextState();
	}

	@Override
	public void handleDialogInteraction(NPC npc) {
		activeState.handleDialogInteraction(npc);
		if (activeState.isRequestingChange())
			activeState = activeState.getNextState();

	}

}
