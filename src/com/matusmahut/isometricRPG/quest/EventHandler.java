package com.matusmahut.isometricRPG.quest;

import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.map.Segment;

public interface EventHandler {

	public void handlePlayerPositionChange(Segment segment);
	
	public void handleDialogInteraction(NPC npc);

	void handleDialogChoice(ConversationType type, int i);
	
	
}
