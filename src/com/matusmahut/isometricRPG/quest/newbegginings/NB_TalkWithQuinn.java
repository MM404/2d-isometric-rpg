package com.matusmahut.isometricRPG.quest.newbegginings;

import java.awt.Point;

import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.Bron;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.FriendlyNPC;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.Quinn;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.quest.ConversationType;
import com.matusmahut.isometricRPG.quest.QuestState;

public class NB_TalkWithQuinn extends QuestState {

	private NPC npc;

	public NB_TalkWithQuinn() {
		super("Talk to the thief");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void handlePlayerPositionChange(Segment segment) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleDialogInteraction(NPC npc) {
		if (npc instanceof Quinn) {
			this.npc = npc;
			player.setConversation(ConversationType.NB_QUINN_DIALOG1, npc);
			player.getMap().getSegmentByMapPoint(new Point(47, 38)).setHighLighted(false);
		}

	}

	@Override
	public void handleDialogChoice(ConversationType type, int i) {
		if (type == ConversationType.NB_QUINN_DIALOG1) {
			if (i == 1) {
				setNextState(new NB_TalkToBron(1));
			} else if (i == 2) {
				//npc.getaSegment().setBlocked(false);
				//npc.setPosition(npc.getMap().getSegmentByMapPoint(new Point(46, 39)));
				
				FriendlyNPC fNPC=(FriendlyNPC)npc;
				fNPC.go(npc.getMap().getSegmentByMapPoint(new Point(46, 39)));
				setNextState(new NB_TalkToBron(2));
			} else if (i == 3) {
				npc.takeDamage(10000);
				setNextState(new NB_TalkToBron(3));
			}
		}

	}

}
