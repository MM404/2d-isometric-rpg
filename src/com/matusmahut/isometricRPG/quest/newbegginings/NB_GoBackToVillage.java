package com.matusmahut.isometricRPG.quest.newbegginings;

import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.quest.ConversationType;
import com.matusmahut.isometricRPG.quest.QuestState;

public class NB_GoBackToVillage extends QuestState {

	public NB_GoBackToVillage() {
		super("Go back to village");
	}

	@Override
	public void handlePlayerPositionChange(Segment segment) {
		if (segment.getXIndex()>=34 && segment.getXIndex()<50) {
			if (segment.getYIndex()>=0 && segment.getYIndex()<11) {
				setNextState(new NB_End());
				player.addXP(1500);
			}
		}

	}

	@Override
	public void handleDialogInteraction(NPC npc) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleDialogChoice(ConversationType type, int i) {
		// TODO Auto-generated method stub

	}

}
