package com.matusmahut.isometricRPG.quest.newbegginings;

import java.awt.Point;
import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.gameObject.units.enemyNPC.Bandit;
import com.matusmahut.isometricRPG.gameObject.units.enemyNPC.EnemyNPC;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.Alice;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.BanditF;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.Bron;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.FriendlyNPC;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.Martin;
import com.matusmahut.isometricRPG.map.Map;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.quest.ConversationType;
import com.matusmahut.isometricRPG.quest.QuestState;

public class NB_TalkToBronStorm extends QuestState {
	int decision;

	public NB_TalkToBronStorm(int i) {
		super("Report to Bron");
		decision = i;
	}

	@Override
	public void handlePlayerPositionChange(Segment segment) {

	}

	@Override
	public void handleDialogInteraction(NPC npc) {
		if (npc instanceof Bron) {
			player.setConversation(ConversationType.NB_BRON_ATTACK, npc);

		}
	}

	@Override
	public void handleDialogChoice(ConversationType type, int i) {

		if (type == ConversationType.NB_BRON_ATTACK) {

			if (i == 1) {
				Map map = player.getMap();
				if (decision<3) {
					map.getSegmentByMapPoint(new Point(21, 5)).setGameObject(new Martin(map));
					FriendlyNPC fnpc = (FriendlyNPC) map.getSegmentByMapPoint(new Point(21, 5)).getGameObject();
					player.getMap().addNPC(fnpc);
					map.getSegmentByMapPoint(new Point(21, 3)).setGameObject(new BanditF(map));
					fnpc = (FriendlyNPC) map.getSegmentByMapPoint(new Point(21, 3)).getGameObject();
					player.getMap().addNPC(fnpc);
					setNextState(new NB_DefendingVillage(decision));
				} else {
					map.getSegmentByMapPoint(new Point(21, 5)).setGameObject(new Bandit(map));
					EnemyNPC enpc = (EnemyNPC) map.getSegmentByMapPoint(new Point(21, 5)).getGameObject();
					enpc.setLifeBar();
					player.getMap().addNPC(enpc);
					map.getSegmentByMapPoint(new Point(21, 3)).setGameObject(new Bandit(map));
					enpc = (EnemyNPC) map.getSegmentByMapPoint(new Point(21, 3)).getGameObject();
					enpc.setLifeBar();
					player.getMap().addNPC(enpc);
					setNextState(new NB_killinBandits());
				}
				if (decision == 3) {
					map.getSegmentByMapPoint(new Point(47, 40))
							.setGameObject(new GameObject(GameObjectType.ALICE_DEAD));

					map.getSegmentByMapPoint(new Point(47, 41)).setGameObject(new Bandit(map));
					EnemyNPC enpc = (EnemyNPC) map.getSegmentByMapPoint(new Point(47, 41)).getGameObject();
					enpc.setLifeBar();
					player.getMap().addNPC(enpc);
				} else {
					map.getSegmentByMapPoint(new Point(48, 43)).setGameObject(new Alice(map));
					FriendlyNPC fnpc = (FriendlyNPC) map.getSegmentByMapPoint(new Point(48, 43)).getGameObject();
					player.getMap().addNPC(fnpc);

				}
				
				
			} else if (i == 2) {
				Map map = player.getMap();
				if (decision == 3) {
					map.getSegmentByMapPoint(new Point(47, 40))
							.setGameObject(new GameObject(GameObjectType.ALICE_DEAD));

					map.getSegmentByMapPoint(new Point(47, 41)).setGameObject(new Bandit(map));
					EnemyNPC enpc = (EnemyNPC) map.getSegmentByMapPoint(new Point(47, 41)).getGameObject();
					enpc.setLifeBar();
					player.getMap().addNPC(enpc);
				} else {
					map.getSegmentByMapPoint(new Point(48, 43)).setGameObject(new Alice(map));
					FriendlyNPC fnpc = (FriendlyNPC) map.getSegmentByMapPoint(new Point(48, 43)).getGameObject();
					player.getMap().addNPC(fnpc);

				}
				setNextState(new NB_FindingAlice(decision));
			}
			player.addXP(300);
		}

	}

}
