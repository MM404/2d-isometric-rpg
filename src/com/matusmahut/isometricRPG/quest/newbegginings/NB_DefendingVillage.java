package com.matusmahut.isometricRPG.quest.newbegginings;


import java.awt.Point;

import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.BanditF;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.Martin;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.Quinn;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.quest.ConversationType;
import com.matusmahut.isometricRPG.quest.QuestState;

public class NB_DefendingVillage extends QuestState {

	private int decision;

	public NB_DefendingVillage(int i) {
		super("Confront bandits in front of village");
		decision = i;
	}

	@Override
	public void handlePlayerPositionChange(Segment segment) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleDialogInteraction(NPC npc) {
		if (npc instanceof Martin) {

			if (decision == 1) {
				player.setConversation(ConversationType.NB_MARTIN_MEET1, npc);
			} else if (decision == 2) {
				player.setConversation(ConversationType.NB_MARTIN_MEET2, npc);
			} else
				player.setConversation(ConversationType.NB_MARTIN_MEET3, npc);
		}

	}

	@Override
	public void handleDialogChoice(ConversationType type, int i) {
		if (decision==1) {
			setNextState(new NB_FindingAlice2(decision));
		} else{
			GameObject gO = player.getMap().getSegmentByMapPoint(new Point(21, 3)).getGameObject();
			if (gO instanceof BanditF) {
				((BanditF) gO).go(player.getMap().getSegmentByMapPoint(new Point(0, 5)));
			}
			gO = player.getMap().getSegmentByMapPoint(new Point(21, 5)).getGameObject();
			if (gO instanceof Martin) {
				((Martin) gO).go(player.getMap().getSegmentByMapPoint(new Point(0, 7)));
			}
			setNextState(new NB_FindingAlice2(decision));
		}
	}

}
