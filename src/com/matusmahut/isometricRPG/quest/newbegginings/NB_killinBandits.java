package com.matusmahut.isometricRPG.quest.newbegginings;

import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.quest.ConversationType;
import com.matusmahut.isometricRPG.quest.QuestState;

public class NB_killinBandits extends QuestState {

	public NB_killinBandits() {
		super("Confront bandits in front of village");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void handlePlayerPositionChange(Segment segment) {
		if (segment.getXIndex() >= 19 && segment.getXIndex() < 24) {
			if (segment.getYIndex() >= 2 && segment.getYIndex() < 9) {
				setNextState(new NB_FindingAlice2(3));
				
			}

		}

	}

	@Override
	public void handleDialogInteraction(NPC npc) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleDialogChoice(ConversationType type, int i) {
		// TODO Auto-generated method stub

	}

}
