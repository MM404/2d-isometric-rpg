package com.matusmahut.isometricRPG.quest.newbegginings;

import java.awt.Point;

import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.Bron;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.quest.ConversationType;
import com.matusmahut.isometricRPG.quest.QuestState;

public class NB_MeetBrom extends QuestState {

	public NB_MeetBrom() {
		super("Talk to villager");
	}

	@Override
	public void handlePlayerPositionChange(Segment segment) {
		// TODO Auto-generated method stub
	}

	
	@Override
	public void handleDialogInteraction(NPC npc) {
		if (npc instanceof Bron) {
			player.setConversation(ConversationType.NB_BRON_INTRO,npc);
			player.getMap().getSegmentByMapPoint(new Point(39, 2)).setHighLighted(false);
		}
		
	}

	@Override
	public void handleDialogChoice(ConversationType type, int i) {
		if (i<3) {
			player.addXP(150);
			this.setNextState(new NB_FindThief());
		}		
	}

}
