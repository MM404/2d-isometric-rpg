package com.matusmahut.isometricRPG.quest.newbegginings;

import java.awt.Point;

import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.gameObject.units.Unit;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.FriendlyNPC;
import com.matusmahut.isometricRPG.map.Map;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.quest.ConversationType;
import com.matusmahut.isometricRPG.quest.QuestState;

public class NB_BeforeStorm extends QuestState {

	int count = 0;
	int decision;

	public NB_BeforeStorm(int i) {
		super("Check out surroundings for suspicious activity");
		decision = i;
	}

	@Override
	public void handlePlayerPositionChange(Segment segment) {
		if (segment.getXIndex() >= 28 && segment.getXIndex() < 50 && segment.getYIndex() >= 0 && segment.getYIndex() < 18) {
			return;
		} else{
			count++;
			if (count > 50) {
				Map map =player.getMap();
				Unit unit = (Unit)(map.getSegmentByMapPoint(new Point(39,3)).getGameObject());
				unit.takeDamage(50000);
				
				setNextState(new NB_TalkToBronStorm(decision));
			}
		}

	}

	@Override
	public void handleDialogInteraction(NPC npc) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleDialogChoice(ConversationType type, int i) {
		// TODO Auto-generated method stub

	}

}
