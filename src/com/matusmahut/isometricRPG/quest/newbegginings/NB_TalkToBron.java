package com.matusmahut.isometricRPG.quest.newbegginings;

import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.Bron;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.quest.ConversationType;
import com.matusmahut.isometricRPG.quest.QuestState;

public class NB_TalkToBron extends QuestState {
	private int decision;

	public NB_TalkToBron(int i) {
		super("Talk to Bron in the village");
		decision=i;
	}

	@Override
	public void handlePlayerPositionChange(Segment segment) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleDialogInteraction(NPC npc) {
		if (npc instanceof Bron) {
			player.setConversation(ConversationType.NB_BRON_SUPPLIES,npc);
		}
		
	}

	@Override
	public void handleDialogChoice(ConversationType type, int i) {

		setNextState(new NB_BeforeStorm(decision));
		
	}

}
