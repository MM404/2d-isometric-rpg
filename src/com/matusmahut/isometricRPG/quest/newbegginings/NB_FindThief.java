package com.matusmahut.isometricRPG.quest.newbegginings;

import java.awt.Point;

import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.quest.ConversationType;
import com.matusmahut.isometricRPG.quest.QuestState;

public class NB_FindThief extends QuestState{

	public NB_FindThief() {
		super("Find thiefs hideout south of the village");
	}

	@Override
	public void handlePlayerPositionChange(Segment segment) {
		if (segment.getXIndex()>=41 && segment.getXIndex()<50) {
			if (segment.getYIndex()>=32 && segment.getYIndex()<46) {
				setNextState(new NB_TalkWithQuinn());
				player.getMap().getSegmentByMapPoint(new Point(47, 38)).setHighLighted(true);
			}
		}
		
	}

	@Override
	public void handleDialogInteraction(NPC npc) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void handleDialogChoice(ConversationType type, int i) {
		// TODO Auto-generated method stub
		
	}

}
