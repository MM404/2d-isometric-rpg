package com.matusmahut.isometricRPG.quest.newbegginings;

import java.awt.Point;

import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.FriendlyNPC;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.Quinn;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.quest.ConversationType;
import com.matusmahut.isometricRPG.quest.QuestState;

public class NB_TalkWithQuinn2 extends QuestState {

	private NPC npc;
	private int decision;

	public NB_TalkWithQuinn2(int decision) {
		super("Talk to Quinn");
		this.decision = decision;
		
	}

	@Override
	public void handlePlayerPositionChange(Segment segment) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleDialogInteraction(NPC npc) {

		if (npc instanceof Quinn) {
			this.npc = npc;
			if (decision == 1) {
				player.setConversation(ConversationType.NB_QUINN_ALICE2, npc);
			} else
				player.setConversation(ConversationType.NB_QUINN_ALICE1, npc);
		}

	}

	@Override
	public void handleDialogChoice(ConversationType type, int i) {
		if (type == ConversationType.NB_QUINN_ALICE1 || type == ConversationType.NB_QUINN_ALICE2) {
			
			 if (i == 1) {
				FriendlyNPC fNPC = (FriendlyNPC) npc;
				fNPC.go(npc.getMap().getSegmentByMapPoint(new Point(46, 39)));
				setNextState(new NB_FreeAlice());
			} else if (i == 2) {
				npc.takeDamage(10000);
				setNextState(new NB_FreeAlice());
			}
			 player.addXP(250);
		}

	}

}
