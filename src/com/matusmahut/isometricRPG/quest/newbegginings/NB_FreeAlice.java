package com.matusmahut.isometricRPG.quest.newbegginings;

import java.awt.Point;

import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.Alice;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.FriendlyNPC;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.quest.ConversationType;
import com.matusmahut.isometricRPG.quest.QuestState;

public class NB_FreeAlice extends QuestState {

	public NB_FreeAlice() {
		super("Free Alice");
	}

	@Override
	public void handlePlayerPositionChange(Segment segment) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleDialogInteraction(NPC npc) {
		if (npc instanceof Alice) {
			((Alice) npc).go(npc.getMap().getSegmentByMapPoint(new Point(39, 3)));
			setNextState(new NB_GoBackToVillage());
		}

	}

	@Override
	public void handleDialogChoice(ConversationType type, int i) {
		// TODO Auto-generated method stub

	}

}
