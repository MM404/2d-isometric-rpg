package com.matusmahut.isometricRPG.quest.newbegginings;

import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.quest.ConversationType;
import com.matusmahut.isometricRPG.quest.QuestState;

public class NB_End extends QuestState {

	public NB_End() {
		super("Quest Completed");
		// TODO Auto-generated constructor stub
	}

	@Override
	public void handlePlayerPositionChange(Segment segment) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleDialogInteraction(NPC npc) {
		// TODO Auto-generated method stub

	}

	@Override
	public void handleDialogChoice(ConversationType type, int i) {
		// TODO Auto-generated method stub

	}

}
