package com.matusmahut.isometricRPG.quest.newbegginings;

import java.awt.Point;

import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.Quinn;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.quest.ConversationType;
import com.matusmahut.isometricRPG.quest.QuestState;

public class NB_FindingAlice2 extends QuestState {

	int decision;

	public NB_FindingAlice2(int i) {
		super("Find Alice south of the village");
		decision = i;
	}

	@Override
	public void handlePlayerPositionChange(Segment segment) {

		if (segment.getXIndex() >= 41 && segment.getXIndex() < 50) {
			if (segment.getYIndex() >= 32 && segment.getYIndex() < 46) {
				
				if (decision == 3) {
					setNextState(new NB_GoBackToVillage());
				} else{
					GameObject gO = player.getMap().getSegmentByMapPoint(new Point(46, 39)).getGameObject();
					if (gO instanceof Quinn) {
						((Quinn) gO).go(player.getMap().getSegmentByMapPoint(new Point(47, 38)));
					}
					setNextState(new NB_TalkWithQuinn2(decision));
				}
					
			}
		}

	}

	@Override
	public void handleDialogInteraction(NPC npc) {

	}

	@Override
	public void handleDialogChoice(ConversationType type, int i) {

	}

}
