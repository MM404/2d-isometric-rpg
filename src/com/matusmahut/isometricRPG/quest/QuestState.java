package com.matusmahut.isometricRPG.quest;

import com.matusmahut.isometricRPG.gameObject.units.Player;

public abstract class QuestState implements EventHandler {

	private boolean isRequestingChange;
	private QuestState nextState;
	protected Player player;
	private String description;

	public QuestState(String description) {
		this.description = description;
	}

	public void setDescription() {
		player.getHud().setQuestDescription(description);
	}

	public boolean isRequestingChange() {
		return isRequestingChange;
	}

	protected void setNextState(QuestState state) {
		isRequestingChange = true;
		nextState = state;
		nextState.setPlayer(player);
		nextState.setDescription();
	}

	public QuestState getNextState() {
		return nextState;
	}

	public void setPlayer(Player player) {
		this.player = player;

	}

}
