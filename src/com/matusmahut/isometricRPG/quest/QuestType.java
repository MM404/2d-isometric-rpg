package com.matusmahut.isometricRPG.quest;

import com.matusmahut.isometricRPG.quest.newbegginings.NB_start;

public enum QuestType {
	NEW_BEGGININGS("New Begginings", new NB_start());

	private final String name;
	private final QuestState state;

	private QuestType(String name, QuestState state) {
		this.name = name;
		this.state = state;
	}

	public String getName() {
		return name;
	}

	public QuestState getState() {
		return state;
	}

}
