package com.matusmahut.isometricRPG.quest;

public enum ConversationType {
	
	NB_BRON_INTRO(new String[]{
			"Hi traveler, I am Bron. I run this village.",
			"We are trying to build community here,",
			"but we have problems with bandits. One",
			"stole our supplies and run south. If you",
			"try to reason with him we will be grateful."}
		, "Sure","Fine, but no promises",""),
	
	NB_BRON_SUPPLIES(new String[]{
			"You got the supplies, good job. I am ",
			"afraid I have some bad news though.",
			"My scout heard some rummors about an ",
			"attack on our settlement, I hate to",
			"bother you again but can you please ",
			"check out the surroundings?,"}
		, "Fine","",""),
	
	NB_BRON_ATTACK(new String[]{
			"They took my daughter Alice. And our ",
			"scout reported bandits are coming here",
			"from East. I dont know what to do."}
		, "I will take care of bandits first","I will find Alice first",""),
	
	NB_QUINN_DIALOG1(new String[]{
			"OH shit, let me guess, Bron sent you.",
			"You must be new here, I dont know what,",
			"Bron told you but we kind of need the ",
			"supplies, city doesnt send anything to",
			"us. Well, I am unarmed and you look ",
			"dangerous, just take the supplies and,",
			"tell Bron to enjoy life while he can."}
		, "Give me the supplies"," Ill take the supplies and all your stuff too","A cant let you leave, you are dead"), 
	
	NB_QUINN_ALICE1(new String[]{
			"Of course it is you again. Girl is fine.",
			"It was just a disctaction so you dont",
			"disturb my friends which are now probably ",
			"killing all the villagers. Take the girl",
			"and get out. You are not going to kill ",
			"unarmed man are you?"}
		, "No, but get out of my way","Oh yes I am, you dont deserve to live",""),
	
	NB_QUINN_ALICE2(new String[]{
			"Of course it is you again. Girl is fine.",
			"It was just a disctaction so you dont",
			"disturb my friends. They will try to ",
			"resolve it peacefuly. You actually made",
			"an impression on us when you let me go ",
			"unharmed."}
		, "Move aside, Im taking her back","Yeah, I let you go first time, now you die",""),
	
	NB_MARTIN_MEET1(new String[]{
			"Look we know you let our friend go.",
			"And we dont want to spil blood. So",
			"just give us small portion of supplies ",
			"every month and we will not bother ",
			"anyone. We need those supplies too, ",
			"you know."}
		, "Fine","",""),
	
	NB_MARTIN_MEET2(new String[]{
			"You must be the guy that robbed Quin.",
			"That was not very nice. Give us half",
			"of your supplies every month and we",
			"will let you live.",}
		, "Fine","Get out of here or I kill you both",""),
	
	NB_MARTIN_MEET3(new String[]{
			"Oh, you must the guy that killed Quin.",
			"Prepare to die"}
		, "Karma is a bitch","",""),;
	
	
	
	
	private final String[] question;
	private final String a1;
	private final String a2;
	private final String a3;
	
	private ConversationType(String[] question, String a1, String a2, String a3) {
		this.question=question;
		this.a1=a1;
		this.a2=a2;
		this.a3=a3;
	}

	public String[] getQuestion() {
		return question;
	}

	public String getA1() {
		return a1;
	}

	public String getA2() {
		return a2;
	}

	public String getA3() {
		return a3;
	}
}
