package com.matusmahut.isometricRPG.gui;

public enum ButtonType {
	
	CANCEL,
	OK,
	TAKE_ALL,
	BACK,
	SLOT,
	PLUS;
}
