package com.matusmahut.isometricRPG.gui;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Rectangle;

import com.matusmahut.isometricRPG.engine.Camera;
import com.matusmahut.isometricRPG.math.Vector3f;

public class Button extends Rectangle {

	/**
	 * 
	 */
	private final ButtonType type;
	private static final long serialVersionUID = 1L;

	public Button(float x, float y, float width, float height, ButtonType type) {
		super((int) x, (int) y, (int) width, (int) height);
		this.type = type;
	}

	public ButtonType getType() {
		return type;
	}
	
	public void renderBounds(){

		glColor3f(1f, 0f, 0f);
		Vector3f cameraPos = Camera.getPosition();
	
		glBegin(GL_LINES);
		
		glVertex2f(this.x-cameraPos.x, this.y-cameraPos.y);
		glVertex2f(this.x-cameraPos.x, this.y + this.height-cameraPos.y);
		glEnd();

		glBegin(GL_LINES);
		glVertex2f(this.x-cameraPos.x, this.y-cameraPos.y);
		glVertex2f(this.x + this.width-cameraPos.x, this.y-cameraPos.y);
		glEnd();

		glBegin(GL_LINES);
		glVertex2f(this.x-cameraPos.x + this.width, this.y-cameraPos.y);
		glVertex2f(this.x + this.width-cameraPos.x, this.y + this.height-cameraPos.y);
		glEnd();

		glBegin(GL_LINES);
		glVertex2f(this.x-cameraPos.x, this.y + this.height-cameraPos.y);
		glVertex2f(this.x-cameraPos.x + this.width, this.y + this.height-cameraPos.y);
		glEnd();
		
	}

}
