package com.matusmahut.isometricRPG.gui;

import com.matusmahut.isometricRPG.gameObject.inventoryObject.InventoryObject;

public class SlotButton extends Button {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private InventoryObject object;

	public SlotButton(float x, float y, float width, float height) {
		super(x, y, width, height, ButtonType.SLOT);

	}

	public void render(){
		if (object!=null) {
			object.render();
		}		
	}

	public void setObject(InventoryObject object) {
		this.object = object;
		
		object.setGameObject(x, -y-height, width, height, 0, 0);
	}

	public InventoryObject getObject() {
		return object;
	}
	
	public void removeObject(){
		object=null;
	}

}
