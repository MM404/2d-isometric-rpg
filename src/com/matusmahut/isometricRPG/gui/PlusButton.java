package com.matusmahut.isometricRPG.gui;

import static org.lwjgl.opengl.GL11.*;

import com.matusmahut.isometricRPG.engine.Camera;
import com.matusmahut.isometricRPG.math.Vector3f;

public class PlusButton extends Button {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4057532493226376731L;

	public PlusButton(float x, float y, float width, float height) {
		super(x, y, width, height, ButtonType.PLUS);
		
	}
	
	public void render(){
		
		Vector3f cameraPos = Camera.getPosition();
		glLineWidth(2);
		glBegin(GL_LINES);
		
		glVertex2f(this.x+this.width/2-cameraPos.x, this.y-cameraPos.y);
		glVertex2f(this.x+this.width/2-cameraPos.x, this.y + this.height-cameraPos.y);
		glEnd();

		glBegin(GL_LINES);
		glVertex2f(this.x-cameraPos.x, this.y-cameraPos.y+this.height/2);
		glVertex2f(this.x + this.width-cameraPos.x, this.y-cameraPos.y+this.height/2);
		glEnd();
		
		glLineWidth(1);
	}

	
}
