package com.matusmahut.isometricRPG.gui;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import org.lwjgl.glfw.GLFW;
import com.matusmahut.isometricRPG.engine.Game;
import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.LootObject;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.InventoryObject;
import com.matusmahut.isometricRPG.gameObject.units.Player;
import com.matusmahut.isometricRPG.math.Vector3f;

public class LootMenu extends GameObject {
	private Button[] buttons;
	private SlotButton[][] slots;
	private Button pressedButton;
	private LootObject lootObject;

	public LootMenu(GameObjectType type, Player player) {
		super(new Vector3f((Game.sWidth / 4.2f) * 3, -Game.sHeight + (Game.sHeight / 4) * 3, 0), type);
		buttons = new Button[2];
		slots = new SlotButton[4][2];
		player.setLootMenu(this);
		init();
	}

	private void init() {
		Button button = new Button((aPosition.x + aSize.x / 20), (-aPosition.y - aSize.y / 5), (aSize.x / 2.3f),
				(aSize.y / 5.5f), ButtonType.CANCEL);
		buttons[0] = button;
		button = new Button((aPosition.x + aSize.x / 1.9f), (-aPosition.y - aSize.y / 5), (aSize.x / 2.3f),
				(aSize.y / 5.5f), ButtonType.TAKE_ALL);
		buttons[1] = button;
		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 2; j++) {
				slots[i][j] = new SlotButton((aPosition.x + aSize.x / 4.3f * i + aSize.x / 20f),
						(-aPosition.y - aSize.y + aSize.y / 3 * j + aSize.y / 10f), (aSize.x / 5f), (aSize.y / 3.1f));

			}

		}

	}

	public void render() {
		super.render();
		if (pressedButton != null) {
			pressedButton.renderBounds();
		}

		for (int i = 0; i < 4; i++) {
			for (int j = 0; j < 2; j++) {
				slots[i][j].render();

			}

		}

	}

	public void processInput(Player player, Point clickedPoint, int mouseButton) {

		Button pressedButton = null;
		for (Button button : buttons) {
			if (button.contains(clickedPoint)) {
				pressedButton = button;
				break;
			}
		}

		if (pressedButton == null) {
			for (int i = 0; i < 4; i++) {
				for (int j = 0; j < 2; j++) {
					if (slots[i][j].contains(clickedPoint)) {
						pressedButton = slots[i][j];
						break;
					}
				}
			}
		}

		if (pressedButton == null) {
			return;
		}

		switch (pressedButton.getType()) {
		case CANCEL:
			this.pressedButton = null;
			player.setLooting(false);
			break;
		case SLOT:
			if (mouseButton == GLFW.GLFW_MOUSE_BUTTON_2) {
				SlotButton slotbutton = (SlotButton) pressedButton;
				if (slotbutton.getObject()== null) 
					return;
				
				player.addToInventory(slotbutton.getObject());
				lootObject.remove(slotbutton.getObject());
				slotbutton.removeObject();
				
			}
			this.pressedButton = pressedButton;
			break;

		default:
			break;
		}

	}

	public void setLoot(LootObject lootObject) {
		
		ArrayList<InventoryObject> objects = lootObject.getInventoryObjects();
		int size = objects.size();
		this.lootObject = lootObject;
		for (int i = 0; i < slots[0].length; i++) {
			
			for (int j = 0; j < slots.length; j++) {

				if (size!=0) {
				slots[j][i].setObject(objects.get(size-1));
				size--;
				} else slots[j][i].removeObject();
				
			}
		}

	}

}
