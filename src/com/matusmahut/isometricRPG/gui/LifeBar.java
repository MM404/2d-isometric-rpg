package com.matusmahut.isometricRPG.gui;

import static org.lwjgl.opengl.GL11.*;

import com.matusmahut.isometricRPG.engine.Camera;
import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.math.Vector2f;
import com.matusmahut.isometricRPG.math.Vector3f;

public class LifeBar {

	private int health;
	private int maxHealth;

	private  Vector2f origPointLT;
	private  Vector2f origPointLB;
	private  Vector2f origPointRB;
	private  Vector2f origPointRT;
	private  Vector2f pointLT;
	private  Vector2f pointLB;
	private  Vector2f maxPointRB;
	private  Vector2f maxPointRT;
	private Vector2f pointRB;
	private Vector2f pointRT;

	public LifeBar(NPC npc, float x, float y, float size) {
		origPointLT = new Vector2f(x, y);
		origPointLB = new Vector2f(x, y + size / 5f);
		origPointRB = new Vector2f(x + size, y + size / 5f);
		origPointRT = new Vector2f(x + size, y);
		pointLT = new Vector2f(x+size/25f, y+size/25f);
		pointLB = new Vector2f(x+size/25f, y-size/25f + size / 5f);
		maxPointRB = new Vector2f(x + size-size/25f, y-size/25f + size / 5f);
		maxPointRT = new Vector2f(x + size-size/25f, y+size/25f);
		pointRB = new Vector2f(x + size-size/25f, y-size/25f + size / 5f);
		pointRT =new Vector2f(x + size-size/25f, y+size/25f);

		health = npc.getHealth();
		maxHealth = npc.getMaxHealth();
		init();
	}

	private void init() {
		calculateHealthBarLength();
	}

	public void render() {

		Vector3f cameraPos = Camera.getPosition();
		// health
		glColor3f(0f, 0f, 0f);
		glBegin(GL_QUADS);
		glVertex3f(origPointLT.x, origPointLT.y,0.3f);
		glVertex3f(origPointLB.x, origPointLB.y,1f);
		glVertex3f(origPointRB.x, origPointRB.y,1f);
		glVertex3f(origPointRT.x, origPointRT.y,1f);
		glEnd();

		glColor3f(1f, 0f, 0f);
		glBegin(GL_QUADS);
		glVertex3f(pointLT.x, pointLT.y,0.2f);
		glVertex3f(pointLB.x, pointLB.y,0.2f);
		glVertex3f(pointRB.x, pointRB.y,0.2f);
		glVertex3f(pointRT.x, pointRT.y,0.2f);
		glEnd();
	}

	private void calculateHealthBarLength() {

		float distX = ((maxPointRT.x) - (pointLT.x)) * ((float) health / (float) maxHealth);
		pointRT.x = pointLT.x + distX;
		pointRB.x = pointLT.x + distX;
	}
	
	public void setPosition(float x, float y, float size){
		origPointLT = new Vector2f(x, y);
		origPointLB = new Vector2f(x, y + size / 5f);
		origPointRB = new Vector2f(x + size, y + size / 5f);
		origPointRT = new Vector2f(x + size, y);
		pointLT = new Vector2f(x+size/25f, y+size/25f);
		pointLB = new Vector2f(x+size/25f, y-size/25f + size / 5f);
		maxPointRB = new Vector2f(x + size-size/25f, y-size/25f + size / 5f);
		maxPointRT = new Vector2f(x + size-size/25f, y+size/25f);
		pointRB = new Vector2f(x + size-size/25f, y-size/25f + size / 5f);
		pointRT =new Vector2f(x + size-size/25f, y+size/25f);
		calculateHealthBarLength();
	}
	

	public void setHealth(int health) {
		this.health = health;
		calculateHealthBarLength();
	}

}
