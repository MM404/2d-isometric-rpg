package com.matusmahut.isometricRPG.gui;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Point;

import org.lwjgl.glfw.GLFW;

import com.matusmahut.isometricRPG.engine.Camera;
import com.matusmahut.isometricRPG.engine.Game;
import com.matusmahut.isometricRPG.engine.state.GameplayState;
import com.matusmahut.isometricRPG.engine.state.StateType;
import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.InventoryObject;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject.WearableInventoryObject;
import com.matusmahut.isometricRPG.gameObject.units.Player;
import com.matusmahut.isometricRPG.graphics.Font;
import com.matusmahut.isometricRPG.graphics.Font.FontType;
import com.matusmahut.isometricRPG.math.Vector2f;
import com.matusmahut.isometricRPG.math.Vector3f;

public class HUD {
	private int toxicity;
	private int health;
	private int maxHealth;
	private int damage;
	private int armor;
	private String questDescription = "";
	private Font font;
	private Font smallFont;
	private Player player;

	private final GameObject healthIcon = new GameObject(GameObjectType.ICON_HEALTH);
	private final GameObject damageIcon = new GameObject(GameObjectType.ICON_DAMAGE);
	private final GameObject armorIcon = new GameObject(GameObjectType.ICON_ARMOR);
	private final PlusButton levelUpButton = new PlusButton(Game.sWidth / 6f + 5f, Game.sHeight / 30f, 18, 18);

	private final Vector2f origPointLT = new Vector2f(Game.sWidth / 30f, Game.sHeight / 30f);
	private final Vector2f origPointLB = new Vector2f(Game.sWidth / 30f, Game.sHeight / 15f);
	private final Vector2f origPointRB = new Vector2f(Game.sWidth / 6f, Game.sHeight / 15f);
	private final Vector2f origPointRT = new Vector2f(Game.sWidth / 6f, Game.sHeight / 30f);
	private final Vector2f pointLT = new Vector2f(Game.sWidth / 28f, Game.sHeight / 28f);
	private final Vector2f pointLB = new Vector2f(Game.sWidth / 28f, Game.sHeight / 15.5f);
	private final Vector2f maxPointRB = new Vector2f(Game.sWidth / 6.05f, Game.sHeight / 15.5f);
	private final Vector2f maxPointRT = new Vector2f(Game.sWidth / 6.05f, Game.sHeight / 28f);
	private Vector2f pointRB = new Vector2f(Game.sWidth / 6.05f, Game.sHeight / 15.5f);
	private Vector2f pointRT = new Vector2f(Game.sWidth / 6.05f, Game.sHeight / 28f);

	public HUD(Player player) {
		player.setHud(this);
		this.player = player;
		toxicity = player.getToxicity();
		health = player.getHealth();
		maxHealth = player.getMaxHealth();
		damage = player.getDamage();
		armor = player.getArmor();

		init();

	}

	private void init() {
		healthIcon.setGameObject(1, -42, 25, 25, 0, 0);
		damageIcon.setGameObject(1, -75, 25, 25, 0, 0);
		armorIcon.setGameObject(1, -105, 25, 25, 0, 0);

		try {
			font = new Font(FontType.ROBOTO_BLACK.getPath(), 20);
			smallFont = new Font(FontType.ROBOTO_BLACK.getPath(), 12);
		} catch (Exception e) {
			e.printStackTrace();
		}

		calculateHealthBarLength();
	}

	public void render() {

		Vector3f cameraPos = Camera.getPosition();

		healthIcon.render();
		damageIcon.render();
		armorIcon.render();
		if (player.getPoints() > 0) {
			glColor3f(0.8f, 0.8f, 0f);
			levelUpButton.render();
		}
		glColor3f(1f, 0f, 0f);
		font.drawTextRelative(String.valueOf(damage), 30, 50);
		font.drawTextRelative(String.valueOf(armor), 30, 80);
		smallFont.drawTextRelative("* "+questDescription, 10, 110);
		glColor3f(0f, 0f, 0f);

		// health
		glBegin(GL_QUADS);
		glVertex2f(origPointLT.x - cameraPos.x, origPointLT.y - cameraPos.y);
		glVertex2f(origPointLB.x - cameraPos.x, origPointLB.y - cameraPos.y);
		glVertex2f(origPointRB.x - cameraPos.x, origPointRB.y - cameraPos.y);
		glVertex2f(origPointRT.x - cameraPos.x, origPointRT.y - cameraPos.y);
		glEnd();

		glColor3f(1f, 0f, 0f);
		glBegin(GL_QUADS);
		glVertex2f(pointLT.x - cameraPos.x, pointLT.y - cameraPos.y);
		glVertex2f(pointLB.x - cameraPos.x, pointLB.y - cameraPos.y);
		glVertex2f(pointRB.x - cameraPos.x, pointRB.y - cameraPos.y);
		glVertex2f(pointRT.x - cameraPos.x, pointRT.y - cameraPos.y);
		glEnd();
		
		

	}

	private void calculateHealthBarLength() {

		float distX = ((maxPointRT.x) - (pointLT.x)) * ((float) health / (float) maxHealth);
		pointRT.x = pointLT.x + distX;
		pointRB.x = pointLT.x + distX;
	}

	public void setToxicity(int toxicity) {
		this.toxicity = toxicity;
	}

	public void setHealth(int health) {
		this.health = health;

		calculateHealthBarLength();
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public void setMaxHeath(int maxHealth) {
		this.maxHealth = maxHealth;
		calculateHealthBarLength();
	}

	public void setArmor(int armor) {
		this.armor = armor;

	}

	public boolean processInput(Point clickedPoint) {

		if (player.getPoints() == 0) {
			return false;
		}
		if (levelUpButton.contains(clickedPoint)) {
			return true;
		} else
			return false;
	}

	public void setQuestDescription(String desc) {
		questDescription = desc;
	}

}
