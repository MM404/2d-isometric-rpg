package com.matusmahut.isometricRPG.gui;

import java.awt.Point;
import java.util.ArrayList;

import org.lwjgl.glfw.GLFW;
import static org.lwjgl.opengl.GL11.*;
import com.matusmahut.isometricRPG.engine.Game;
import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.InventoryObject;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject.Armor;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject.Weapon;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject.WearableInventoryObject;
import com.matusmahut.isometricRPG.gameObject.units.Player;
import com.matusmahut.isometricRPG.graphics.Font;
import com.matusmahut.isometricRPG.graphics.Font.FontType;
import com.matusmahut.isometricRPG.math.Vector3f;

public class Inventory extends GameObject {
	public SlotButton[][] slots;
	public ArrayList<Button> buttons = new ArrayList<Button>();
	private Button pressedButton;
	private Font bigFont;
	private Font normalFont;

	public Inventory(Player player) {
		super(new Vector3f(Game.sWidth / 4f, -Game.sHeight, 0), GameObjectType.UI_INVENTORY);
		slots = new SlotButton[8][4];
		player.setInventory(this);
		try {
			bigFont = new Font(FontType.ROBOTO_BLACK.getPath(), 25);
			normalFont = new Font(FontType.ROBOTO_BLACK.getPath(), 15);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		init();
	}

	private void init() {
		/*
		 * Button button= new Button((aPosition.x + aSize.x/20),
		 * (-aPosition.y-aSize.y/5), (aSize.x/2.3f), (aSize.y/5.5f),
		 * ButtonType.CANCEL); buttons.add(button); button= new
		 * Button((aPosition.x + aSize.x/1.9f), (-aPosition.y-aSize.y/5),
		 * (aSize.x/2.3f), (aSize.y/5.5f), ButtonType.TAKE_ALL);
		 * buttons.add(button);
		 */

		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 4; j++) {
				slots[i][j] = new SlotButton((aPosition.x + aSize.x / 8.65f * i + aSize.x / 17f),
						(-aPosition.y - aSize.y + aSize.y / 13 * j + aSize.y / 1.65f), (aSize.x / 10f),
						(aSize.y / 14f));
			}

		}
	}

	public void render() {
		super.render();
		if (pressedButton != null) {
			pressedButton.renderBounds();
			renderInfo();
		}
		for (int i = 0; i < slots.length; i++) {
			for (int j = 0; j < slots[0].length; j++) {
				slots[i][j].render();
			}
		}

	}

	private void renderInfo() {
		if (pressedButton instanceof SlotButton) {
			SlotButton slotButton = (SlotButton) pressedButton;
			InventoryObject invObject = slotButton.getObject();
			if (invObject == null)
				return;
			glColor3f(0, 0, 0);
			bigFont.drawTextRelative(invObject.getName(), (int) (this.aPosition.x + aSize.x / 3.3f),
					(int) (-this.aPosition.y - aSize.y + 50));
			if (invObject instanceof Armor) {
				Armor armor = (Armor) invObject;
				normalFont.drawTextRelative("Armor: ", (int) (this.aPosition.x + aSize.x / 3.3f),
						(int) (-this.aPosition.y - aSize.y + 100));
				normalFont.drawTextRelative("" + armor.getArmor(), (int) (this.aPosition.x + aSize.x / 3.3f + 150),
						(int) (-this.aPosition.y - aSize.y + 100));
			} else if (invObject instanceof Weapon) {
				Weapon weapon = (Weapon) invObject;
				normalFont.drawTextRelative("Damage: ", (int) (this.aPosition.x + aSize.x / 3.3f),
						(int) (-this.aPosition.y - aSize.y + 100));
				normalFont.drawTextRelative("" + weapon.getDamage(), (int) (this.aPosition.x + aSize.x / 3.3f + 150),
						(int) (-this.aPosition.y - aSize.y + 100));
				normalFont.drawTextRelative("Attack Time: ", (int) (this.aPosition.x + aSize.x / 3.3f),
						(int) (-this.aPosition.y - aSize.y + 150));
				normalFont.drawTextRelative("" +((double)weapon.getBaseAttackTime()/1000)+"s", (int) (this.aPosition.x + aSize.x / 3.3f + 150),
						(int) (-this.aPosition.y - aSize.y + 150));
			}
		}

	}

	public void processInput(Player player, Point clickedPoint, int mouseButton) {

		Button pressedButton = null;
		for (Button button : buttons) {
			if (button.contains(clickedPoint)) {
				pressedButton = button;
				break;
			}
		}

		if (pressedButton == null) {
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 4; j++) {
					if (slots[i][j].contains(clickedPoint)) {
						pressedButton = slots[i][j];
						break;
					}
				}
			}
		}

		if (pressedButton == null) {
			return;
		}

		this.pressedButton = pressedButton;

		switch (pressedButton.getType()) {
		case CANCEL:
			pressedButton = null;
			player.setLooting(false);
			break;
		case SLOT:
			if (mouseButton == GLFW.GLFW_MOUSE_BUTTON_2) {
				InventoryObject iObject = ((SlotButton) pressedButton).getObject();
				if (iObject != null) {

					if (iObject instanceof WearableInventoryObject) {
						player.wear(iObject);

					} else {
						iObject.use(player);
						((SlotButton) pressedButton).removeObject();
					}
				}

			}
			break;

		default:
			break;
		}

	}

	public void addObject(InventoryObject object) {
		if (object == null) {
			return;
		}
		for (int i = 0; i < slots[0].length; i++) {
			for (int j = 0; j < slots.length; j++) {
				if (slots[j][i].getObject() == null) {
					slots[j][i].setObject(object);
					return;
				}
			}
		}

	}

}
