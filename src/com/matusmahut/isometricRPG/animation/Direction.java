package com.matusmahut.isometricRPG.animation;

public enum Direction {

	NORTH, SOUTH, EAST, WEST, NORTH_EAST, NORTH_WEST, SOUTH_EAST, SOUTH_WEST;

	//vr�ti da��� smer, bu� v smere alebo proti smeru hodinov�ch ru�i�iek
	public static Direction getNext(Direction direction, boolean clockWise) {
		switch (direction) {
		case NORTH:
			if (clockWise)
				return NORTH_EAST;
			else
				return NORTH_WEST;

		case NORTH_EAST:
			if (clockWise)
				return EAST;
			else
				return NORTH;

		case NORTH_WEST:
			if (clockWise)
				return NORTH;
			else
				return WEST;

		case EAST:
			if (clockWise)
				return SOUTH_EAST;
			else
				return NORTH_EAST;

		case WEST:
			if (clockWise)
				return NORTH_WEST;
			else
				return SOUTH_WEST;

		case SOUTH_EAST:
			if (clockWise)
				return SOUTH;
			else
				return EAST;

		case SOUTH_WEST:
			if (clockWise)
				return WEST;
			else
				return SOUTH;

		case SOUTH:
			if (clockWise)
				return SOUTH_WEST;
			else
				return SOUTH_EAST;

		default:
			break;
		}
		return null;
	}

}
