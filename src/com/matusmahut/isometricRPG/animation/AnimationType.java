package com.matusmahut.isometricRPG.animation;

import java.util.HashMap;

import com.matusmahut.isometricRPG.gameObject.units.Player.AnimationPart;

public enum AnimationType {

	// NAME(PATH, IDLE loopLength, WALK loopLength, ATTACK loopLength, DEATH loopLength, starting Direction, clockWise, is parsed)
	HERO("resources/Hero/",0,10,10,8,Direction.SOUTH_EAST, false,true,AnimationPart.ALL),
	ALICE("resources/Alice/",4,8,6,8,Direction.WEST,true,false,AnimationPart.ALL),	
	BANDIT("resources/Bandit/",4,8,6,8,Direction.WEST,true,false,AnimationPart.ALL),	
	MINOTAUR("resources/Minotaur/",4,8,6,8,Direction.WEST,true,true,AnimationPart.ALL),
	SKELETON("resources/Skeleton/",4,8,6,8,Direction.WEST,true,false,AnimationPart.ALL),
	SKELETON_KNIGHT("resources/SkeletonKnight/",4,8,6,8,Direction.WEST,true,false,AnimationPart.ALL),
	SKELETON_MAGE("resources/SkeletonMage/",4,8,6,8,Direction.WEST,true,false,AnimationPart.ALL),
	ZOMBIE("resources/Zombie/",4,8,6,8,Direction.WEST,true,false,AnimationPart.ALL),
	//GOBLIN("resources/Minotaur/",4,8,6,8,Direction.WEST,true,true,AnimationPart.ALL),
	// HERO
	HERO_CLOTHES("resources/isometric_hero/clothes/",4,8,6,8,Direction.WEST,true,false,AnimationPart.BODY),
	HERO_HEAD1("resources/isometric_hero/head1/",4,8,6,8,Direction.WEST,true,false,AnimationPart.HEAD),
	HERO_HEAD2("resources/isometric_hero/head2/",4,8,6,8,Direction.WEST,true,false,AnimationPart.HEAD),
	HERO_HEAD3("resources/isometric_hero/head3/",4,8,6,8,Direction.WEST,true,false,AnimationPart.HEAD),
	HERO_ARMOR_LEATHER("resources/isometric_hero/armor_leather/",4,8,6,8,Direction.WEST,true,false,AnimationPart.BODY),
	HERO_ARMOR_STEEL("resources/isometric_hero/armor_steel/",4,8,6,8,Direction.WEST,true,false,AnimationPart.BODY),
	HERO_WEAPON_DAGGER("resources/isometric_hero/dagger/",4,8,6,8,Direction.WEST,true,false,AnimationPart.WEAPON),
	HERO_WEAPON_SHORTSWORD("resources/isometric_hero/shortsword/",4,8,6,8,Direction.WEST,true,false,AnimationPart.WEAPON),
	HERO_WEAPON_LONGSWORD("resources/isometric_hero/longsword/",4,8,6,8,Direction.WEST,true,false,AnimationPart.WEAPON),
	HERO_WEAPON_GREATSWORD("resources/isometric_hero/greatsword/",4,8,6,8,Direction.WEST,true,false,AnimationPart.WEAPON),
	HERO_SHIELD_WOOD("resources/isometric_hero/buckler/",4,8,6,8,Direction.WEST,true,false,AnimationPart.WEAPON),
	HERO_SHIELD_STEEL("resources/isometric_hero/shield/",4,8,6,8,Direction.WEST,true,false,AnimationPart.WEAPON);
	
	private final String path;
	private final int lengthIdle;
	private final int lengthWalk;
	private final int lengthAttack;
	private final int lengthDeath;
	private final AnimationPart part;
	private final HashMap<Direction, Integer> directions;
	private final boolean isParsed;
	
	private AnimationType(String path, int lengthIdle, int lengthWalk, int lengthAttack, int lengthDeath, Direction direction, boolean clockWise, boolean isParsed, AnimationPart part) {
		this.path=path;
		this.part=part;
		this.lengthIdle=lengthIdle;
		this.lengthWalk=lengthWalk;
		this.lengthAttack=lengthAttack;
		this.lengthDeath=lengthDeath;
		this.directions = new HashMap<>();
		this.isParsed=isParsed;
		
		directions.put(direction, 0);
		for (int i = 1; i < 8; i++) {
			directions.put(direction=Direction.getNext(direction,clockWise), i);
		}
	}
	
	public int getDirectionMultiplicator(Direction dir){
		return directions.get(dir);
	}

	public String getPath() {
		return path;
	}

	public int getLengthIdle() {
		return lengthIdle;
	}

	public int getLengthWalk() {
		return lengthWalk;
	}

	public int getLengthAttack() {
		return lengthAttack;
	}

	public int getLengthDeath() {
		return lengthDeath;
	}
	
	public boolean isParsed() {
		return isParsed;
	}

	public AnimationPart getPart() {
		return part;
	}
	
	
	
}
