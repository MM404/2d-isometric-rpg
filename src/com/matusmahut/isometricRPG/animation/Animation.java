package com.matusmahut.isometricRPG.animation;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

import com.matusmahut.isometricRPG.gameObject.units.Player;
import com.matusmahut.isometricRPG.gameObject.units.Player.AnimationPart;
import com.matusmahut.isometricRPG.gameObject.units.Unit;
import com.matusmahut.isometricRPG.graphics.Texture;

public class Animation {

	private ArrayList<Texture> walkingTextures = new ArrayList<>();
	private ArrayList<Texture> idleTextures = new ArrayList<>();
	private ArrayList<Texture> deathTextures = new ArrayList<>();
	private ArrayList<Texture> celebrationTextures = new ArrayList<>();
	private ArrayList<Texture> attackTextures = new ArrayList<>();
	private ArrayList<Texture> activeTextures = new ArrayList<>();
	private long lastChange = 0;
	private final long timeOut = 100;
	private long bat;
	private int loopLength = 10;
	private int start = 0;
	private int pointer = 0;
	private Unit unit;
	private Direction direction = Direction.NORTH;
	private AnimationType type;
	private AnimationAction action;
	private AnimationPart part;

	public Animation(Unit unit, AnimationType type) {
		this.unit = unit;
		this.type = type;
		this.part = type.getPart();
		this.bat = unit.getBat();
		if (!type.isParsed()) {
			smartInit();
		} else {
			init();
		}
	}

	public void setBAT(long bat) {

		if (unit.getAgility() != 0) {
			bat -= (bat / 100) * unit.getAgility();
		}
		this.bat = bat;
	}

	//inicializ�cia anm�cie ak s� obr�zky rozdelen� do prie�inkov pod�a akcie
	private void init() {
		File f = new File(type.getPath() + "Walk/");
		ArrayList<String> strings = new ArrayList<String>(Arrays.asList(f.list()));
		for (String string : strings) {
			this.walkingTextures.add(new Texture(type.getPath() + "Walk/" + string));
		}
		if (type.getLengthIdle() == 0) {

			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < type.getLengthWalk(); j++) {
					idleTextures.add(walkingTextures.get(i * loopLength));
				}
			}
		} else {
			f = new File(type.getPath() + "Idle/");
			strings = new ArrayList<String>(Arrays.asList(f.list()));
			for (String string : strings) {
				this.idleTextures.add(new Texture(type.getPath() + "Idle/" + string));
			}
		}

		f = new File(type.getPath() + "Attack/");
		strings = new ArrayList<String>(Arrays.asList(f.list()));
		for (String string : strings) {
			this.attackTextures.add(new Texture(type.getPath() + "Attack/" + string));
		}
		setAction(AnimationAction.IDLE);

	}

	//inicializ�cia anm�cie ak s� obr�zky v jednom s�bore
	private void smartInit() {
		File f = new File(type.getPath() + "All/");
		System.out.println(f);
		ArrayList<String> strings = new ArrayList<String>(Arrays.asList(f.list()));
		int size = strings.get(0).length();
		int number = 0;
		for (String string : strings) {
			if (string.length() == size) {
				number = Integer.valueOf(string.substring(size - 5, size - 4));
			} else {
				number = Integer.valueOf(string.substring(size - 5, size - 3));
			}
			if (isBetween(number, 0, 3)) {
				this.idleTextures.add(new Texture(type.getPath() + "All/" + string));
			} else if (isBetween(number, 4, 11)) {
				this.walkingTextures.add(new Texture(type.getPath() + "All/" + string));
			} else if (isBetween(number, 12, 17)) {
				this.attackTextures.add(new Texture(type.getPath() + "All/" + string));
			} else if (isBetween(number, 18, 23)) {
				this.deathTextures.add(new Texture(type.getPath() + "All/" + string));
			} else if (isBetween(number, 24, 31)) {
				this.celebrationTextures.add(new Texture(type.getPath() + "All/" + string));
			}

		}
		setAction(AnimationAction.IDLE);

	}

	//v�mena akt�vneho obr�zka
	public void update() {

		if (activeTextures == attackTextures) {
			if (System.currentTimeMillis() - lastChange < bat/type.getLengthAttack())
				return;
		} else if (System.currentTimeMillis() - lastChange < timeOut)
			return;

		if (activeTextures == idleTextures || activeTextures == walkingTextures) {
			if (pointer == start + loopLength)
				pointer = start;
			setTexture();
			lastChange = System.currentTimeMillis();
			pointer++;
		} else {
			if (pointer >= start + loopLength) {
				pointer = start;
				attackDone();
				setAction(AnimationAction.IDLE);
			}

			setTexture();
			lastChange = System.currentTimeMillis();
			pointer++;

		}

	}

	private void attackDone() {
		if (part == AnimationPart.BODY || part == AnimationPart.ALL) {
			unit.attackDone();
		}
	}

	private void setTexture() {
		if (unit instanceof Player) {
			((Player) unit).setTexture(activeTextures.get(pointer), part);
		} else {
			unit.setTexture(activeTextures.get(pointer));
		}
	}

	public void attack(Unit unit) {
		activeTextures = attackTextures;
	}

	public void setAction(AnimationAction type) {
		if (this.action == type) {
			return;
		}

		action = type;

		switch (type) {
		case IDLE:
			activeTextures = idleTextures;
			loopLength = this.type.getLengthIdle();
			if (loopLength == 0) {
				loopLength = this.type.getLengthWalk();
			}

			break;
		case WALK:
			activeTextures = walkingTextures;
			loopLength = this.type.getLengthWalk();
			break;
		case ATTACK:
			activeTextures = attackTextures;
			loopLength = this.type.getLengthAttack();
			break;

		default:
			break;
		}
		setDirection(direction);
	}

	public void setDirection(Direction direction) {
		if (action == AnimationAction.WALK) {
			int temp = pointer - start;
			start = type.getDirectionMultiplicator(direction) * loopLength;
			pointer = start + temp;
		} else {
			start = type.getDirectionMultiplicator(direction) * loopLength;
			pointer = start;
		}

		this.direction = direction;
	}

	public AnimationAction getAction() {
		return action;
	}

	public static boolean isBetween(int x, int lower, int upper) {
		return lower <= x && x <= upper;
	}

	public AnimationType getType() {
		return type;
	}

	public Direction getDirection() {
		return direction;
	}

}
