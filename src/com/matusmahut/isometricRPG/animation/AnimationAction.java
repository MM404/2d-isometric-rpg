package com.matusmahut.isometricRPG.animation;

public enum AnimationAction {
	IDLE,
	WALK,
	DEATH,
	ATTACK;
}
