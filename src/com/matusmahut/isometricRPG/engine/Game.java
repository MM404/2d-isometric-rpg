package com.matusmahut.isometricRPG.engine;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.opengl.GL;
import org.lwjgl.opengl.GL11;
import com.matusmahut.isometricRPG.engine.input.CursorHandler;
import com.matusmahut.isometricRPG.engine.input.KeyHandler;
import com.matusmahut.isometricRPG.engine.input.MouseClickHandler;
import com.matusmahut.isometricRPG.engine.state.AbstractState;
import com.matusmahut.isometricRPG.engine.state.ControlsState;
import com.matusmahut.isometricRPG.engine.state.CreditsState;
import com.matusmahut.isometricRPG.engine.state.DiedState;
import com.matusmahut.isometricRPG.engine.state.GameplayState;
import com.matusmahut.isometricRPG.engine.state.InventoryState;
import com.matusmahut.isometricRPG.engine.state.LevelState;
import com.matusmahut.isometricRPG.engine.state.MenuState;
import com.matusmahut.isometricRPG.engine.state.StateManager;
import com.matusmahut.isometricRPG.engine.state.StateType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.graphics.Shader;
import com.matusmahut.isometricRPG.gui.HUD;
import com.matusmahut.isometricRPG.gui.LootMenu;
import com.matusmahut.isometricRPG.map.Map;
import com.matusmahut.isometricRPG.math.Matrix4f;
import com.matusmahut.isometricRPG.quest.QuestType;

public class Game {
	public static final float sWidth = 800;
	public static final float sHeight = 600;
	StateManager stateManager;

	public Game() {

	}

	private void init() {
		// creating bounding to GLWF

		GL.createCapabilities();
		glMatrixMode(GL11.GL_PROJECTION);
		glLoadIdentity();
		Matrix4f pr_matrix = Matrix4f.orthographic(0, sWidth, 0, -sHeight, 1, -1);
		glLoadMatrixf(pr_matrix.toFloatBuffer());
		// glOrtho(0, sWidth, 0, -sHeight, 1, -1);
		glMatrixMode(GL11.GL_MODELVIEW);
		glClearColor(1f, 1f, 1f, 0f);

		//nacitanie a nastavenie shaderov
		Shader.loadAll();
		Shader.RELATIVE.enable();
		Shader.RELATIVE.setUniformMat4f("pr_matrix", pr_matrix);
		Shader.RELATIVE.disable();
		Shader.STATIC.enable();
		Shader.STATIC.setUniformMat4f("pr_matrix", pr_matrix);
		Shader.STATIC.disable();

		//vytvorenie hlavn�ch hern�ch ent�t
		Map map = new Map(50, 50, 50);
		LootMenu lootMenu = new LootMenu(GameObjectType.UI_LOOT, map.getPlayer());
		HUD hud = new HUD(map.getPlayer());
		map.getPlayer().setQuest(QuestType.NEW_BEGGININGS);

		//vytvorenie stavov, ktor� musia by� zapamatan�
		stateManager = new StateManager();
		stateManager.putState(new GameplayState(map, lootMenu, hud));
		stateManager.putState(new InventoryState(map, hud));
		stateManager.putState(new LevelState(map.getPlayer()));
		stateManager.putState(new MenuState());
		stateManager.putState(new ControlsState());
		stateManager.putState(new DiedState(map));
		stateManager.putState(new CreditsState());
		stateManager.setActiveState(StateType.MENU);
		map.getPlayer().setGameStateManager(stateManager);

		glTranslatef(0, -sHeight, 0f);
	}

	public void run(long window, KeyHandler keyHandler, CursorHandler cursorHandler,
			MouseClickHandler mouseClickHandler) {

		init();

		long startCountdown = System.currentTimeMillis();
		int counter = 0;
		// be�� k�m sa nezavrie okno
		while (glfwWindowShouldClose(window) == GLFW_FALSE) {

			glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // cistenie buffrov

			AbstractState currentState = stateManager.getActiveState();
			// process input
			glfwPollEvents();
			currentState.processInput(keyHandler, cursorHandler, mouseClickHandler);
			
			//update
			currentState.update();
			
			glEnable(GL_BLEND);
			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
			currentState.render(); // kreslenie

			glfwSwapBuffers(window); // swap color buffrov

			

			// fps po��tadlo
			counter++;
			if (System.currentTimeMillis() - startCountdown > 1000) {
				// System.out.println("FPS: " + counter);
				counter = 0;
				startCountdown = System.currentTimeMillis();
			}

		}
	}

}