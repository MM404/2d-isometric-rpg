package com.matusmahut.isometricRPG.engine.state;

import com.matusmahut.isometricRPG.engine.input.CursorHandler;
import com.matusmahut.isometricRPG.engine.input.KeyHandler;
import com.matusmahut.isometricRPG.engine.input.MouseClickHandler;

public interface State {

	public void render();
	
	public void update();	
	
	public void wakeUp();
	
	public void processInput(KeyHandler inputListener, CursorHandler cursorHandler,
			MouseClickHandler mouseClickHandler);
	
}
