package com.matusmahut.isometricRPG.engine.state;

import java.util.HashMap;

import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.map.Map;
import com.matusmahut.isometricRPG.quest.ConversationType;

public class StateManager {
	private AbstractState activeState;
	private HashMap<StateType, AbstractState> states = new HashMap<StateType, AbstractState>();

	public StateManager() {

	}

	public void putState(AbstractState state) {
		states.put(state.getType(), state);
		activeState = state;
	}

	public AbstractState getActiveState() {
		if (activeState.isRequestingChange()) {
			activeState = states.get(activeState.getNextStateType());
			activeState.wakeUp();
		}
		return activeState;
	}

	public void setActiveState(StateType type) {
		this.activeState = states.get(type);
	}

	public void setActiveState(ConversationType type, Map map, NPC npc) {
		this.activeState = new DialogState(map, type, npc);
	}

}
