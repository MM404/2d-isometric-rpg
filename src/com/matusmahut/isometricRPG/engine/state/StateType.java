package com.matusmahut.isometricRPG.engine.state;

public enum StateType {

	INVENTORY,
	GAMEPLAY,
	LEVELING,
	DIALOG,
	MENU,
	MESSAGE, 
	CONTROLS,
	CREDITS, 
	DIED;
	
}
