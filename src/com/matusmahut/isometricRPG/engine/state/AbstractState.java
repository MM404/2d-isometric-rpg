package com.matusmahut.isometricRPG.engine.state;

public abstract class AbstractState implements State {

	private boolean requestChange;
	private StateType type;
	private StateType nextStateType;
	
	public AbstractState(StateType type){
		this.type =  type;
	}
	
	public StateType getType(){
		return this.type;
	}

	public boolean isRequestingChange() {
		return requestChange;
	}
	
	public StateType getNextStateType(){
		StateType ret = nextStateType;
		nextStateType=null;
		requestChange=false;
		return ret;
	}
	
	protected void requestChange(StateType type){	
		nextStateType=type;
		requestChange=true;
	}
}
