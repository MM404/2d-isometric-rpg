package com.matusmahut.isometricRPG.engine.state;

import static org.lwjgl.glfw.GLFW.*;

import java.awt.Point;

import com.matusmahut.isometricRPG.engine.Game;
import com.matusmahut.isometricRPG.engine.input.CursorHandler;
import com.matusmahut.isometricRPG.engine.input.KeyHandler;
import com.matusmahut.isometricRPG.engine.input.MouseClickHandler;
import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gui.Button;
import com.matusmahut.isometricRPG.gui.ButtonType;

public class ControlsState extends AbstractState {
	private GameObject controls;
	private Button buttonBack;

	public ControlsState() {
		super(StateType.CONTROLS);
		controls = new GameObject(GameObjectType.UI_CONTROLS);
		controls.setGameObject(0, -Game.sHeight, Game.sWidth, Game.sHeight, 0, 0);
		buttonBack = new Button(270, 460, 250, 75, ButtonType.OK);

	}

	public void wakeUp() {

	}

	public void render() {
		controls.render();
	}

	public void update() {

	}

	public void processInput(KeyHandler inputListener, CursorHandler cursorHandler,
			MouseClickHandler mouseClickHandler) {

		// process keyboard input
		int key = inputListener.getReleasedKey();
		if (key == GLFW_KEY_I) {

		} else if (key == GLFW_KEY_J) {

		} else if (key == GLFW_KEY_ESCAPE) {

		}

		// process mouse input
		Point clickedPoint = cursorHandler.getPoint();

		if (mouseClickHandler.isClicked()) {
			if (buttonBack.contains(clickedPoint)) {
				this.requestChange(StateType.MENU);
			}
		}

	}
}

