package com.matusmahut.isometricRPG.engine.state;

import static org.lwjgl.glfw.GLFW.*;

import java.awt.Point;

import com.matusmahut.isometricRPG.engine.Camera;
import com.matusmahut.isometricRPG.engine.input.CursorHandler;
import com.matusmahut.isometricRPG.engine.input.KeyHandler;
import com.matusmahut.isometricRPG.engine.input.MouseClickHandler;
import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.Interactable;
import com.matusmahut.isometricRPG.gameObject.units.Player;
import com.matusmahut.isometricRPG.gui.HUD;
import com.matusmahut.isometricRPG.gui.LootMenu;
import com.matusmahut.isometricRPG.map.Map;
import com.matusmahut.isometricRPG.map.Segment;

public class GameplayState extends AbstractState {

	private Map aMap;
	private LootMenu lootMenu;
	private HUD aHud;
	private Player player;

	public GameplayState(Map map, LootMenu lootMenu, HUD hud) {
		super(StateType.GAMEPLAY);
		aMap = map;
		this.lootMenu = lootMenu;
		aHud = hud;
		player = aMap.getPlayer();
	}

	public void render() {
		aMap.render();
		aHud.render();
		if (player.isLooting()) {
			lootMenu.render();
		}
	}

	public void update() {
		if (!player.isLooting()) {
			aMap.update();
		}
		if (player.isDead()) {
			requestChange(StateType.DIED);
		}
	}

	public void processInput(KeyHandler inputListener, CursorHandler cursorHandler,
			MouseClickHandler mouseClickHandler) {

		// process keyboard input
		int key = inputListener.getReleasedKey();
		if (key == GLFW_KEY_I) {
			if (!aMap.getPlayer().isLooting()) 
			requestChange(StateType.INVENTORY);
		} else if (key == GLFW_KEY_J) {
			
		} else if (key == GLFW_KEY_ESCAPE) {
			requestChange(StateType.MENU);
		} else if (key == GLFW_KEY_L) {
			if (!aMap.getPlayer().isLooting()) 
			requestChange(StateType.LEVELING);
		}

		// process mouse input
		Point clickedPoint = cursorHandler.getPoint();
		
		if (mouseClickHandler.isClicked()) {

			if (!aMap.getPlayer().isLooting()) {

				if (aHud.processInput(clickedPoint)){
					requestChange(StateType.LEVELING);
					return;
				}
				
				clickedPoint.setLocation(clickedPoint.getX() - Camera.getPosition().x,
						clickedPoint.getY() - Camera.getPosition().y);
				Segment clickedSegment = aMap.getSegmentByScreenPoint(clickedPoint);
				if (clickedSegment==null) 
					return;
				
				aMap.highlightSegment(clickedPoint);
				GameObject clickedObject = clickedSegment.getGameObject();

				if (clickedObject instanceof Interactable) {
					Interactable lb = (Interactable ) clickedObject;
					lb.interact(aMap.getPlayer());
				}  else
					aMap.findPath(clickedPoint);
			} else {

				lootMenu.processInput(aMap.getPlayer(), clickedPoint, mouseClickHandler.getButton());

			}
		}

	}

	public void wakeUp() {
		
	}

}
