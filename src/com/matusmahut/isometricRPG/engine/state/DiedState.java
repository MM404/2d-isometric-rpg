package com.matusmahut.isometricRPG.engine.state;

import static org.lwjgl.glfw.GLFW.*;

import com.matusmahut.isometricRPG.engine.input.CursorHandler;
import com.matusmahut.isometricRPG.engine.input.KeyHandler;
import com.matusmahut.isometricRPG.engine.input.MouseClickHandler;
import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.map.Map;

public class DiedState extends AbstractState {
	private Map map;
	private boolean reload;
	private GameObject go;

	public DiedState(Map map) {
		super(StateType.DIED);
		this.map = map;
		go= new GameObject(GameObjectType.UI_GAMEOVER);
		go.setGameObject(275, -150, 250, 70, 0, 0);
	}

	@Override
	public void render() {
		map.render();
		go.render();
	}

	@Override
	public void update() {
		map.update();
	}

	@Override
	public void wakeUp() {
		// TODO Auto-generated method stub

	}

	@Override
	public void processInput(KeyHandler inputListener, CursorHandler cursorHandler,
			MouseClickHandler mouseClickHandler) {
		// process keyboard input
				int key = inputListener.getReleasedKey();
				if (key == GLFW_KEY_Q) {
					glfwTerminate();
					System.exit(0);
				} else if (key == GLFW_KEY_R) {
					//setReload(true);
				}

	}

	public boolean isReload() {
		return reload;
	}

	public void setReload(boolean reload) {
		this.reload = reload;
	}

}
