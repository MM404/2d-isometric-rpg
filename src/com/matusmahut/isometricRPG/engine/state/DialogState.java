package com.matusmahut.isometricRPG.engine.state;

import com.matusmahut.isometricRPG.engine.Camera;
import com.matusmahut.isometricRPG.engine.Game;
import com.matusmahut.isometricRPG.engine.input.CursorHandler;
import com.matusmahut.isometricRPG.engine.input.KeyHandler;
import com.matusmahut.isometricRPG.engine.input.MouseClickHandler;
import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.graphics.Font;
import com.matusmahut.isometricRPG.graphics.Font.FontType;
import com.matusmahut.isometricRPG.gui.Button;
import com.matusmahut.isometricRPG.gui.ButtonType;
import com.matusmahut.isometricRPG.map.Map;
import com.matusmahut.isometricRPG.quest.ConversationType;
import com.matusmahut.isometricRPG.quest.Quest;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Point;

public class DialogState extends AbstractState {

	private Map map;
	private GameObject dialog;
	private GameObject dialogOptions;
	private Button button1;
	private Button button2;
	private Button button3;
	private ConversationType type;
	private NPC npc;
	private Font font;
	private Font bigFont;
	private String[] question;

	public DialogState(Map map, ConversationType type, NPC npc) {
		super(StateType.DIALOG);
		this.map = map;
		this.type = type;
		this.npc = npc;
		this.dialog = new GameObject(GameObjectType.UI_DIALOG);
		this.dialog.setGameObject(npc.getaSegment().getPoint().x + Camera.getPosition().x,
				-(npc.getaSegment().getPoint().y + Camera.getPosition().y), 210f, 150f, 0, 0);
		this.dialogOptions = new GameObject(GameObjectType.UI_DIALOG_OPTIONS);
		this.dialogOptions.setGameObject(Game.sWidth / 3 + 40f, -Game.sHeight / 0.9f, 400f, 200f, 0, 0);
		this.button1 = new Button(200f, 385f, 370f, 50f, ButtonType.OK);
		this.button2 = new Button(200f, 445f, 370f, 50f, ButtonType.OK);
		this.button3 = new Button(200f, 505f, 370f, 50f, ButtonType.OK);

		this.question = type.getQuestion();
		try {
			this.font = new Font(FontType.ROBOTO_BLACK.getPath(), 10);
			this.bigFont = new Font(FontType.ROBOTO_BLACK.getPath(), 16);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void render() {
		map.render();
		dialog.render();
		dialogOptions.render();
		glColor3f(0f, 0f, 0f);
		for (int i = 0; i < question.length; i++) {
			// font.drawTextRelative(question[i],
			// (int)(npc.getaSegment().getPoint().x+Camera.getPosition().x),
			// (int)((npc.getaSegment().getPoint().y+Camera.getPosition().y)));
			font.drawText(question[i], (int) (npc.getaSegment().getPoint().x - 105f),
					(int) ((npc.getaSegment().getPoint().y - 215f + i * 15)));
		}
		if (!type.getA1().isEmpty()) {
			bigFont.drawTextRelative("1. " + type.getA1(), (int) Game.sWidth / 3 - 50, (int) Game.sHeight / 2 + 100);
		}
		if (!type.getA2().isEmpty()) {
			bigFont.drawTextRelative("2. " + type.getA2(), (int) Game.sWidth / 3 - 50, (int) Game.sHeight / 2 + 160);
		}
		if (!type.getA3().isEmpty()) {
			bigFont.drawTextRelative("3. " + type.getA3(), (int) Game.sWidth / 3 - 50, (int) Game.sHeight / 2 + 210);
		}
		button1.renderBounds();
		button2.renderBounds();
		button3.renderBounds();
	}

	@Override
	public void update() {
		map.update();

	}

	@Override
	public void wakeUp() {

	}

	@Override
	public void processInput(KeyHandler inputListener, CursorHandler cursorHandler,
			MouseClickHandler mouseClickHandler) {
		if (mouseClickHandler.isClicked()) {
			Point clickedPoint = cursorHandler.getPoint();
			Quest quest = map.getPlayer().getQuest();
			if (button1.contains(clickedPoint)) {
				if (type.getA1().isEmpty()) 
					return;
				quest.handleDialogChoice(type, 1);
				requestChange(StateType.GAMEPLAY);
			} else if (button2.contains(clickedPoint)) {
				if (type.getA2().isEmpty()) 
					return;
				quest.handleDialogChoice(type, 2);
				requestChange(StateType.GAMEPLAY);
			} else if (button3.contains(clickedPoint)) {
				if (type.getA3().isEmpty()) 
					return;
				quest.handleDialogChoice(type, 3);
				requestChange(StateType.GAMEPLAY);
			}
		}

	}

}
