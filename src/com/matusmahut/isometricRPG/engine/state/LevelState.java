package com.matusmahut.isometricRPG.engine.state;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

import java.awt.Point;
import java.util.ArrayList;

import com.matusmahut.isometricRPG.engine.Camera;
import com.matusmahut.isometricRPG.engine.Game;
import com.matusmahut.isometricRPG.engine.input.CursorHandler;
import com.matusmahut.isometricRPG.engine.input.KeyHandler;
import com.matusmahut.isometricRPG.engine.input.MouseClickHandler;
import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.units.Player;
import com.matusmahut.isometricRPG.graphics.Font;
import com.matusmahut.isometricRPG.graphics.Font.FontType;
import com.matusmahut.isometricRPG.gui.Button;
import com.matusmahut.isometricRPG.gui.PlusButton;
import com.matusmahut.isometricRPG.math.Vector3f;

public class LevelState extends AbstractState {

	private Player player;
	private Font mediumfont;
	private Font bigFont;
	private Font smallFont;
	private GameObject panel;
	private GameObject strengthIcon;
	private GameObject agilityIcon;
	private GameObject inteligenceIcon;
	private GameObject vitalityIcon;
	private PlusButton addStrengthButton;
	private PlusButton addAgilityButton;
	private PlusButton addInteligenceButton;
	private PlusButton addVitalityButton;
	private final Vector3f panelPos = new Vector3f(Game.sWidth / 4f, -Game.sHeight, 0);
	private ArrayList<Button> buttons;

	public LevelState(Player p) {
		super(StateType.LEVELING);
		player = p;
		try {
			mediumfont = new Font(FontType.ROBOTO_BLACK.getPath(), 18);
			smallFont = new Font(FontType.ROBOTO_BLACK.getPath(), 17);
			bigFont = new Font(FontType.ROBOTO_BLACK.getPath(), 30);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		buttons = new ArrayList<>();
		panel = new GameObject(panelPos, GameObjectType.UI_LEVELPANEL);
		float iconSize = 40;
		float posX = panelPos.x + 100;
		float posY = panelPos.y + 430;

		strengthIcon = new GameObject(GameObjectType.ICON_STRENGTH);
		strengthIcon.setGameObject(posX, posY, iconSize, iconSize, 0, 0);
		addStrengthButton = new PlusButton(posX + 180, -posY - iconSize + 13, 20, 20);
		buttons.add(addStrengthButton);

		posY -= 50;
		agilityIcon = new GameObject(panelPos, GameObjectType.ICON_AGILITY);
		agilityIcon.setGameObject(posX, posY, iconSize, iconSize, 0, 0);
		addAgilityButton = new PlusButton(posX + 180, -posY - iconSize + 13, 20, 20);
		buttons.add(addAgilityButton);

		posY -= 50;
		inteligenceIcon = new GameObject(panelPos, GameObjectType.ICON_INTELIGENCE);
		inteligenceIcon.setGameObject(posX, posY, iconSize, iconSize, 0, 0);
		addInteligenceButton = new PlusButton(posX + 180, -posY - iconSize + 13, 20, 20);
		buttons.add(addInteligenceButton);

		posY -= 50;
		vitalityIcon = new GameObject(panelPos, GameObjectType.ICON_VITALITY);
		vitalityIcon.setGameObject(posX, posY, iconSize, iconSize, 0, 0);
		addVitalityButton = new PlusButton(posX + 180, -posY - iconSize + 13, 20, 20);
		buttons.add(addVitalityButton);

	}

	@Override
	public void render() {
		Vector3f camera = Camera.getPosition();
		if (player.getPoints() > 0)
			glColor3f(0f, 1f, 0f);
		panel.render();
		strengthIcon.render();
		addStrengthButton.render();
		agilityIcon.render();
		addAgilityButton.render();
		vitalityIcon.render();
		addVitalityButton.render();
		inteligenceIcon.render();
		addInteligenceButton.render();
		glColor3f(0f, 0f, 0f);
		bigFont.drawText("LEVEL ", (int) panelPos.x + 130-(int)camera.x, -(int) panelPos.y - 570-(int)camera.y);
		bigFont.drawText("" + player.getLevel(), (int) panelPos.x + 250-(int)camera.x, -(int) panelPos.y - 570-(int)camera.y);
		smallFont.drawText("Available Points: ", (int) panelPos.x + 100-(int)camera.x, -(int) panelPos.y - 520-(int)camera.y);
		if (player.getPoints() > 0)
			glColor3f(0f, 1f, 0f);
		mediumfont.drawText("" + player.getPoints(), (int) panelPos.x + 245-(int)camera.x, -(int) panelPos.y - 520-(int)camera.y);
		glColor3f(0f, 0f, 0f);
		mediumfont.drawText("Strength: ", (int) panelPos.x + 150-(int)camera.x, -(int) panelPos.y - 460-(int)camera.y);
		mediumfont.drawText("" + player.getStrength(), (int) panelPos.x + 250-(int)camera.x, -(int) panelPos.y - 459-(int)camera.y);
		mediumfont.drawText("Agility: ", (int) panelPos.x + 150-(int)camera.x, -(int) panelPos.y - 410-(int)camera.y);
		mediumfont.drawText("" + player.getAgility(), (int) panelPos.x + 250-(int)camera.x, -(int) panelPos.y - 409-(int)camera.y);
		mediumfont.drawText("Inteligence: ", (int) panelPos.x + 150-(int)camera.x, -(int) panelPos.y - 360-(int)camera.y);
		mediumfont.drawText("" + player.getInteligence(), (int) panelPos.x + 250-(int)camera.x, -(int) panelPos.y - 359-(int)camera.y);
		mediumfont.drawText("Vitality: ", (int) panelPos.x + 150-(int)camera.x, -(int) panelPos.y - 310-(int)camera.y);
		mediumfont.drawText("" + player.getVitality(), (int) panelPos.x + 250-(int)camera.x, -(int) panelPos.y - 309-(int)camera.y);

	}

	@Override
	public void update() {

	}

	@Override
	public void wakeUp() {

	}

	@Override
	public void processInput(KeyHandler inputListener, CursorHandler cursorHandler,
			MouseClickHandler mouseClickHandler) {

		// process keyboard input
		int key = inputListener.getReleasedKey();
		if (key == GLFW_KEY_ESCAPE) {
			requestChange(StateType.GAMEPLAY);
		} else if (key == GLFW_KEY_L) {
			requestChange(StateType.GAMEPLAY);
		}

		// process mouse input
		Point clickedPoint = cursorHandler.getPoint();

		if (mouseClickHandler.isClicked()) {

			Button pressedButton = null;
			for (Button button : buttons) {
				if (button.contains(clickedPoint)) {
					pressedButton = button;
					break;
				}
			}

			if (pressedButton == null) {
				return;
			}

			switch (pressedButton.getType()) {
			case PLUS:
				if (player.getPoints()==0)
					return;
					
				if (pressedButton == addAgilityButton) {
					player.addAgility();
				} else if (pressedButton == addStrengthButton) {
					player.addStrength();
				} else if (pressedButton == addInteligenceButton) {
					player.addInteligence();
				} else if (pressedButton == addVitalityButton) {
					player.addVitality();
				}
				player.takePoints(1);
				break;

			default:
				break;
			}
		}

	}

}
