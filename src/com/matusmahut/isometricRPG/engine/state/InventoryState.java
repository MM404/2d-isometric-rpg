package com.matusmahut.isometricRPG.engine.state;

import static org.lwjgl.glfw.GLFW.*;

import java.awt.Point;

import com.matusmahut.isometricRPG.engine.input.CursorHandler;
import com.matusmahut.isometricRPG.engine.input.KeyHandler;
import com.matusmahut.isometricRPG.engine.input.MouseClickHandler;
import com.matusmahut.isometricRPG.gui.HUD;
import com.matusmahut.isometricRPG.gui.Inventory;
import com.matusmahut.isometricRPG.map.Map;

public class InventoryState extends AbstractState {
	private Map aMap;
	private HUD aHud;
	private Inventory inventory;

	public InventoryState(Map map, HUD hud) {
		super(StateType.INVENTORY);
		aMap = map;
		aHud = hud;
		inventory = new Inventory(map.getPlayer());

	}

	public void wakeUp() {
		
	}

	public void render() {
		inventory.render();
	}

	public void update() {

	}

	public void processInput(KeyHandler inputListener, CursorHandler cursorHandler,
			MouseClickHandler mouseClickHandler) {

		// process keyboard input
		int key = inputListener.getReleasedKey();
		if (key == GLFW_KEY_I) {
			requestChange(StateType.GAMEPLAY);
		} else if (key == GLFW_KEY_J) {

		} else if (key == GLFW_KEY_ESCAPE) {
			requestChange(StateType.GAMEPLAY);
		}

		// process mouse input
		Point clickedPoint = cursorHandler.getPoint();

		if (mouseClickHandler.isClicked()) {
			inventory.processInput(aMap.getPlayer(), clickedPoint, mouseClickHandler.getButton());
		}

	}
}
