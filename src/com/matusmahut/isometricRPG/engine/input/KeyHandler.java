package com.matusmahut.isometricRPG.engine.input;

import org.lwjgl.glfw.GLFWKeyCallback;
import static org.lwjgl.glfw.GLFW.*;

public class KeyHandler extends GLFWKeyCallback {

	private int key = -1;
	private int releasedKey = -1;

	@Override
	public void invoke(long window, int key, int scancode, int action, int mods) {

		if (GLFW_PRESS == action) {
			this.key = key;
		} else if (GLFW_RELEASE == action && this.key == key) {
			this.key = -1;
			releasedKey=key;
		}

	}

	public int getPressedKey() {
		return key;
	}
	
	public int getReleasedKey(){
		int ret= releasedKey;
		releasedKey=-1;
		return ret;
	}

}
