package com.matusmahut.isometricRPG.engine.input;

import java.awt.Point;

import org.lwjgl.glfw.GLFWCursorPosCallback;

public class CursorHandler extends GLFWCursorPosCallback {

	Point aPoint = new Point();

	@Override
	public void invoke(long window, double xpos, double ypos) {

		aPoint.setLocation((int) xpos, (int) ypos);
	}

	public Point getPoint() {

		return aPoint;
	}

}