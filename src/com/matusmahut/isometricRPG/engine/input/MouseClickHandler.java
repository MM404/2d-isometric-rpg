package com.matusmahut.isometricRPG.engine.input;

import org.lwjgl.glfw.GLFWMouseButtonCallback;
import static org.lwjgl.glfw.GLFW.*;

public class MouseClickHandler extends GLFWMouseButtonCallback {

	private boolean clicked;
	private int button;

	@Override
	public void invoke(long window, int button, int action, int mods) {

		if (action == GLFW_RELEASE) {
			clicked = true;
			this.button = button;
		}
	}

	public boolean isClicked() {
		boolean ret = clicked;
		clicked = false;
		return ret;
	}

	public int getButton() {
		return button;
	}

}
