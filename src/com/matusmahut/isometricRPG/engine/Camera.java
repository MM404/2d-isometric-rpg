package com.matusmahut.isometricRPG.engine;

import org.lwjgl.opengl.GL11;

import com.matusmahut.isometricRPG.graphics.Shader;
import com.matusmahut.isometricRPG.math.Matrix4f;
import com.matusmahut.isometricRPG.math.Vector3f;

public class Camera {

	public static Vector3f position = new Vector3f(0f, 0f, 0f);

	public static void move2f(float moveX, float moveY) {
		position.x += moveX;
		position.y += moveY;
		//posun mriezky
		GL11.glTranslatef(moveX, moveY, 0f);
		//posun statického shadera
		Shader.STATIC.setUniformMat4f("ml_matrix", Matrix4f.translate(new Vector3f(position.x, position.y, 0)));
	}
	
	public static void move3f(float moveX, float moveY, float moveZ) {
		position.x += moveX;
		position.y += moveY;
		position.y += moveZ;
		//posun mriezky
		GL11.glTranslatef(moveX, moveY, moveZ);
		//posun statického shadera
		Shader.STATIC.setUniformMat4f("ml_matrix", Matrix4f.translate(new Vector3f(position.x, position.y, position.z)));
	}

	public static Vector3f getPosition() {
		return position;
	}

}