package com.matusmahut.isometricRPG.map;

import static com.matusmahut.isometricRPG.gameObject.GameObjectType.*;

import java.awt.Point;
import java.util.ArrayList;
import java.util.LinkedList;
import com.matusmahut.isometricRPG.engine.Game;
import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.LootObject;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.usableObject.Potion_Damage;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.usableObject.Potion_Health;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject.ChainMailArmor;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject.Dagger;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject.GreatSword;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject.LeatherArmor;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject.LongSword;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject.PaddedArmor;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject.PlateArmor;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject.ShortSword;
import com.matusmahut.isometricRPG.gameObject.units.Player;
import com.matusmahut.isometricRPG.gameObject.units.Unit;
import com.matusmahut.isometricRPG.gameObject.units.enemyNPC.EnemyNPC;
import com.matusmahut.isometricRPG.gameObject.units.enemyNPC.KnightSkeleton;
import com.matusmahut.isometricRPG.gameObject.units.enemyNPC.Minotaur;
import com.matusmahut.isometricRPG.gameObject.units.enemyNPC.Skeleton;
import com.matusmahut.isometricRPG.gameObject.units.enemyNPC.SkeletonMage;
import com.matusmahut.isometricRPG.gameObject.units.enemyNPC.Zombie;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.Alice;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.Bron;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.FriendlyNPC;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.Quinn;
import com.matusmahut.isometricRPG.graphics.Font;
import com.matusmahut.isometricRPG.graphics.Shader;
import com.matusmahut.isometricRPG.graphics.Font.FontType;
import com.matusmahut.isometricRPG.math.Matrix4f;
import com.matusmahut.isometricRPG.math.Vector3f;

public class Map {

	private Segment[][] aSegments;
	private ArrayList<Segment> aSelection;
	private ArrayList<Unit> units = new ArrayList<>();
	private int aHeight;
	private int aWidth;
	private int aSegmentSize;
	private Point aPlayerPos;
	private Player aPlayer;
	private boolean isVillageDestroyed;

	public Map(int paWidth, int paHeight, int paSegmentSize) {

		aHeight = paHeight;
		aWidth = paWidth;
		aSegmentSize = paSegmentSize;
		aSegments = new Segment[aWidth][aHeight];
		aSelection = new ArrayList<Segment>();
		aPlayer = new Player(PLAYER, this);
		PathFinder.setArea(aSegments);
		init();

	}

	public void init() {
		Font font = null;
		try {
			font = new Font(FontType.ROBOTO_BLACK.getPath(), 10);
		} catch (Exception e) {
			e.printStackTrace();
		}

		for (int i = 0; i < aSegments.length; i++) {

			for (int j = 0; j < aSegments.length; j++) {
				aSegments[i][j] = new Segment(i, j, aSegmentSize);
				aSegments[i][j].setFont(font);
			}
		}

		initializeStaticWorld();
		// loot objects
		LootObject lootObject = new LootObject(CHEST_CLOSED_1, aSegments[0][1]);
		lootObject.addLoot(new PaddedArmor());
		aSegments[0][0].setGameObject(lootObject);

		lootObject = new LootObject(CHEST_CLOSED_1, aSegments[25][5]);
		lootObject.addLoot(new Dagger());
		aSegments[25][4].setGameObject(lootObject);

		lootObject = new LootObject(CHEST_CLOSED_1, aSegments[46][1]);
		lootObject.addLoot(new Potion_Damage());
		lootObject.addLoot(new Potion_Health());
		lootObject.addLoot(new Potion_Health());
		aSegments[46][0].setGameObject(lootObject);

		lootObject = new LootObject(CHEST_CLOSED_1, aSegments[46][1]);
		lootObject.addLoot(new Potion_Damage());
		lootObject.addLoot(new Potion_Health());
		lootObject.addLoot(new Potion_Health());
		aSegments[46][0].setGameObject(lootObject);

		lootObject = new LootObject(CHEST_CLOSED_1, aSegments[46][42]);
		lootObject.addLoot(new ChainMailArmor());
		lootObject.addLoot(new Potion_Health());
		aSegments[46][41].setGameObject(lootObject);

		lootObject = new LootObject(CHEST_CLOSED_1, aSegments[30][29]);
		lootObject.addLoot(new LeatherArmor());
		lootObject.addLoot(new ShortSword());
		lootObject.addLoot(new Potion_Health());
		lootObject.addLoot(new Potion_Damage());
		aSegments[30][28].setGameObject(lootObject);

		lootObject = new LootObject(CHEST_CLOSED_1, aSegments[7][48]);
		lootObject.addLoot(new GreatSword());
		aSegments[7][49].setGameObject(lootObject);

		lootObject = new LootObject(CHEST_CLOSED_1, aSegments[19][49]);
		lootObject.addLoot(new PlateArmor());
		aSegments[19][48].setGameObject(lootObject);

		lootObject = new LootObject(CHEST_CLOSED_1, aSegments[4][24]);
		lootObject.addLoot(new LongSword());
		lootObject.addLoot(new Potion_Health());
		lootObject.addLoot(new Potion_Health());
		lootObject.addLoot(new Potion_Health());
		aSegments[4][23].setGameObject(lootObject);

		// units
		aSegments[39][2].setGameObject(new Bron(VILLAGER_W, this));
		FriendlyNPC npc = (FriendlyNPC) aSegments[39][2].getGameObject();
		units.add(npc);

		aSegments[47][38].setGameObject(new Quinn(this));
		npc = (FriendlyNPC) aSegments[47][38].getGameObject();
		units.add(npc);
		
		aSegments[39][3].setGameObject(new Alice(this));
		npc = (FriendlyNPC) aSegments[39][3].getGameObject();
		units.add(npc);

		EnemyNPC enpc;

		aSegments[3][18].setGameObject(new Skeleton(this));
		enpc = (EnemyNPC) aSegments[3][18].getGameObject();
		enpc.setLifeBar();
		units.add(enpc);

		aSegments[22][30].setGameObject(new Skeleton(this));
		enpc = (EnemyNPC) aSegments[22][30].getGameObject();
		enpc.setLifeBar();
		units.add(enpc);

		aSegments[38][42].setGameObject(new Skeleton(this));
		enpc = (EnemyNPC) aSegments[38][42].getGameObject();
		enpc.setLifeBar();
		units.add(enpc);

		aSegments[32][30].setGameObject(new SkeletonMage(this));
		enpc = (EnemyNPC) aSegments[32][30].getGameObject();
		enpc.setLifeBar();
		units.add(enpc);

		aSegments[6][47].setGameObject(new KnightSkeleton(this));
		enpc = (EnemyNPC) aSegments[6][47].getGameObject();
		enpc.setLifeBar();
		units.add(enpc);

		aSegments[5][43].setGameObject(new Minotaur(this));
		enpc = (EnemyNPC) aSegments[5][43].getGameObject();
		enpc.setLifeBar();
		units.add(enpc);

		aSegments[17][47].setGameObject(new Minotaur(this));
		enpc = (EnemyNPC) aSegments[17][47].getGameObject();
		enpc.setLifeBar();
		units.add(enpc);

		aSegments[6][24].setGameObject(new Zombie(this));
		enpc = (EnemyNPC) aSegments[6][24].getGameObject();
		enpc.setLifeBar();
		units.add(enpc);

		// player
		aSegments[10][2].setGameObject(aPlayer);
		aPlayer.initAnimations();
		aPlayerPos = aSegments[10][2].getPoint();
		units.add(aPlayer);

	}

	//renderuj� sa len segmenty v okol� hr��a
	public void render() {

		//rendering surface
		for (int i = aPlayer.getaSegment().getYIndex() - (int) (Game.sHeight / 2) / aSegmentSize
				- 5; i < aPlayer.getaSegment().getYIndex() + (int) (Game.sHeight / 2) / aSegmentSize + 5; i++) {

			if (i < 0 || i >= aSegments.length)
				continue;

			for (int j = aPlayer.getaSegment().getXIndex() - (int) (Game.sWidth / 2) / aSegmentSize
					- 5; j < aPlayer.getaSegment().getXIndex() + (int) (Game.sWidth / 2) / aSegmentSize + 5; j++) {
				if (j < 0 || j >= aSegments.length)
					continue;
				aSegments[j][i].renderSurface();
			}
		}

		//rendering objects
		for (int i = aPlayer.getaSegment().getYIndex() - (int) (Game.sHeight / 2) / aSegmentSize
				- 5; i < aPlayer.getaSegment().getYIndex() + (int) (Game.sHeight / 2) / aSegmentSize + 5; i++) {

			if (i < 0 || i >= aSegments.length)
				continue;

			for (int j = aPlayer.getaSegment().getXIndex() - (int) (Game.sWidth / 2) / aSegmentSize
					- 5; j < aPlayer.getaSegment().getXIndex() + (int) (Game.sWidth / 2) / aSegmentSize + 5; j++) {
				if (j < 0 || j >= aSegments.length)
					continue;
				aSegments[j][i].render();
			}
		}

		/*
		 * for (int i = 0; i < aSegments.length; i++) {
		 * 
		 * for (int j = 0; j < aSegments.length; j++) {
		 * 
		 * aSegments[j][i].render(); } }
		 */
		//vykreslovanie lifebarov
		for (Unit unit : units) {
			if (unit instanceof EnemyNPC) {
				((EnemyNPC) unit).renderLifeBar();
			}
		}

	}
	
	public void update() {

		ArrayList<Unit> toRemove = new ArrayList<>();
		for (Unit unit : units) {
			unit.update();
			if (unit.isDead()) {
				toRemove.add(unit);
				unit.getaSegment().dispose();
			}
		}
		units.removeAll(toRemove);

	}


	//vrati segment podla mapy
	public Segment getSegmentByMapPoint(Point point) {

		if (point.x < 0 || point.y < 0) {
			return null;
		}
		return aSegments[point.x][point.y];
	}
	
	//vrati segment podla suradnic obrazovky
	public Segment getSegmentByScreenPoint(Point point) {

		point = screenToMap(point);
		if (point.getX() < 0 || point.getY() < 0 || point.getX() > aWidth - 1 || point.getY() > aHeight - 1) {
			return null;
		}
		return aSegments[point.x][point.y];
	}

	//prevedenie bodu z obrazovky(input) na mapove suradnice(samotn0 segmenty) 
	//musime to prepocitat preotoze pouzivane izometriu
	private Point screenToMap(Point point) {
		int x = (int) ((point.x / (double) aSegmentSize + point.y / (double) (aSegmentSize / 2)) / 2);
		int y = (int) ((point.y / (double) (aSegmentSize / 2) - point.x / 50.0) / 2);
		return new Point(x, y);
	}

	public void clearSelection() {
		for (Segment segment : aSelection) {
			segment.setFilled(false);
		}
		aSelection.clear();
	}

	public void adjustPosition(float posX, float posY) {

		Shader.STATIC.setUniformMat4f("ml_matrix", Matrix4f.translate(new Vector3f(posX, posY, 0)));

	}

	public Point getPlayerPos() {

		return aPlayerPos;
	}

	public void highlightSegments(LinkedList<Segment> list) {
		clearSelection();
		if (list == null) {
			System.out.println("LIST JE NULL");
			return;
		}
		for (Segment segment : list) {
			segment.changeColor(new Vector3f(1f, 0f, 0f));
			segment.setFilled(true);
			aSelection.add(segment);
		}

	}

	public void highlightSegment(Point point) {

		clearSelection();
		point = screenToMap(point);
		point = screenToMap(point);
		if (point.getX() < 0 || point.getY() < 0 || point.getX() > aWidth - 1 || point.getY() > aHeight - 1) {
			return;
		}
		aSegments[point.x][point.y].changeColor(new Vector3f(1f, 0f, 0f));
		aSegments[point.x][point.y].setFilled(true);
		aSelection.add(aSegments[point.x][point.y]);

	}

	public void findPath(Point clickPoint) {

		clickPoint = screenToMap(clickPoint);
		Segment segment = getSegmentByMapPoint(clickPoint);
		aPlayer.setPath(segment);
		aPlayer.setLockedNPC(null);
		// highlightSegments(playerPath);

	}

	public Player getPlayer() {
		return aPlayer;
	}

	private void initializeStaticWorld() {
		//prostredie, rucne
		aSegments[3][2].setGameObject(new GameObject(TREE_1));
		aSegments[3][4].setGameObject(new GameObject(TREE_1));
		aSegments[5][1].setGameObject(new GameObject(TREE_2));
		aSegments[3][8].setGameObject(new GameObject(TREE_1));
		aSegments[6][8].setGameObject(new GameObject(TREE_1));
		aSegments[14][14].setGameObject(new GameObject(TREE_1));
		aSegments[9][12].setGameObject(new GameObject(TREE_2));
		aSegments[1][13].setGameObject(new GameObject(TREE_1));
		aSegments[9][0].setGameObject(new GameObject(TREE_2));
		aSegments[10][9].setGameObject(new GameObject(TREE_1));
		aSegments[11][2].setGameObject(new GameObject(TREE_1));
		aSegments[8][5].setGameObject(new GameObject(PAVILION));
		aSegments[0][9].setGameObject(new GameObject(TREE_1));
		aSegments[0][14].setGameObject(new GameObject(TREE_1));
		aSegments[0][22].setGameObject(new GameObject(TREE_1));
		aSegments[0][33].setGameObject(new GameObject(TREE_1));
		aSegments[0][48].setGameObject(new GameObject(TREE_1));
		aSegments[4][39].setGameObject(new GameObject(TREE_1));
		aSegments[3][25].setGameObject(new GameObject(TREE_1));
		aSegments[3][44].setGameObject(new GameObject(TREE_1));
		aSegments[3][40].setGameObject(new GameObject(TREE_1));
		aSegments[6][40].setGameObject(new GameObject(TREE_1));
		aSegments[3][38].setGameObject(new GameObject(TREE_1));
		aSegments[14][1].setGameObject(new GameObject(TREE_1));
		aSegments[18][2].setGameObject(new GameObject(TREE_1));
		aSegments[13][10].setGameObject(new GameObject(TREE_1));
		aSegments[21][9].setGameObject(new GameObject(TREE_1));
		aSegments[16][11].setGameObject(new GameObject(TREE_1));
		aSegments[17][9].setGameObject(new GameObject(TREE_1));
		aSegments[21][1].setGameObject(new GameObject(TREE_2));
		aSegments[23][1].setGameObject(new GameObject(TREE_1));
		aSegments[24][10].setGameObject(new GameObject(TREE_1));
		aSegments[26][9].setGameObject(new GameObject(TREE_1));
		aSegments[24][4].setGameObject(new GameObject(TREE_1));
		aSegments[26][4].setGameObject(new GameObject(TREE_2));
		aSegments[28][10].setGameObject(new GameObject(TREE_1));
		aSegments[28][11].setGameObject(new GameObject(TREE_2));
		aSegments[30][12].setGameObject(new GameObject(TREE_1));
		aSegments[33][11].setGameObject(new GameObject(TREE_1));
		aSegments[37][10].setGameObject(new GameObject(TREE_2));
		aSegments[35][11].setGameObject(new GameObject(TREE_2));
		aSegments[31][11].setGameObject(new GameObject(TREE_1));
		aSegments[25][0].setGameObject(new GameObject(TREE_1));
		aSegments[27][0].setGameObject(new GameObject(TREE_1));
		aSegments[28][1].setGameObject(new GameObject(TREE_1));
		aSegments[29][0].setGameObject(new GameObject(TREE_1));
		aSegments[31][0].setGameObject(new GameObject(TREE_1));
		aSegments[33][1].setGameObject(new GameObject(TREE_1));
		aSegments[39][12].setGameObject(new GameObject(TREE_2));
		aSegments[40][13].setGameObject(new GameObject(TREE_1));
		aSegments[42][14].setGameObject(new GameObject(TREE_1));
		aSegments[44][15].setGameObject(new GameObject(TREE_1));
		aSegments[46][16].setGameObject(new GameObject(TREE_1));
		aSegments[47][16].setGameObject(new GameObject(TREE_1));
		aSegments[48][17].setGameObject(new GameObject(TREE_2));
		aSegments[49][16].setGameObject(new GameObject(TREE_1));
		aSegments[49][19].setGameObject(new GameObject(TREE_1));
		aSegments[35][0].setGameObject(new GameObject(TREE_1));
		aSegments[38][1].setGameObject(new GameObject(TREE_1));
		aSegments[40][0].setGameObject(new GameObject(TREE_1));
		aSegments[43][0].setGameObject(new GameObject(TREE_1));
		aSegments[45][1].setGameObject(new GameObject(TREE_2));
		aSegments[48][0].setGameObject(new GameObject(TREE_1));
		aSegments[49][2].setGameObject(new GameObject(TREE_1));
		aSegments[48][4].setGameObject(new GameObject(TREE_1));
		aSegments[49][5].setGameObject(new GameObject(TREE_1));
		aSegments[48][7].setGameObject(new GameObject(TREE_2));
		aSegments[49][11].setGameObject(new GameObject(TREE_1));
		aSegments[48][12].setGameObject(new GameObject(TREE_1));
		aSegments[48][14].setGameObject(new GameObject(TREE_1));
		aSegments[33][5].setGameObject(new GameObject(TREE_1));
		aSegments[36][0].setGameObject(new GameObject(TREE_1));
		aSegments[22][13].setGameObject(new GameObject(TREE_2));
		aSegments[45][42].setGameObject(new GameObject(TREE_1));
		aSegments[46][43].setGameObject(new GameObject(TREE_1));
		aSegments[44][40].setGameObject(new GameObject(TREE_1));
		aSegments[46][38].setGameObject(new GameObject(TREE_1));
		aSegments[48][38].setGameObject(new GameObject(TREE_1));
		aSegments[48][45].setGameObject(new GameObject(TREE_1));
		aSegments[48][46].setGameObject(new GameObject(TREE_1));

		// village
		aSegments[43][14].setGameObject(new GameObject(OUTPOST_SW));
		aSegments[33][3].setGameObject(new GameObject(OUTPOST_NW));
		aSegments[33][7].setGameObject(new GameObject(OUTPOST_NW));
		aSegments[37][3].setGameObject(new GameObject(WELL_SE));
		aSegments[44][9].setGameObject(new GameObject(WELL_SW));
		aSegments[43][5].setGameObject(new GameObject(PAVILION));
		aSegments[39][7].setGameObject(new GameObject(PAVILION));
		aSegments[40][3].setGameObject(new GameObject(TIMBER1));
		aSegments[36][7].setGameObject(new GameObject(TIMBER2));
		aSegments[42][9].setGameObject(new GameObject(TENT1));
		aSegments[40][4].setGameObject(new GameObject(BARRELS1));
		aSegments[39][8].setGameObject(new GameObject(BARRELS1));
		aSegments[44][5].setGameObject(new GameObject(CRATES1));
		aSegments[37][1].setGameObject(new GameObject(CRATES1));
		aSegments[42][2].setGameObject(new GameObject(TABLE2));
		aSegments[39][11].setGameObject(new GameObject(GameObjectType.VILLAGER_NE));
		aSegments[33][6].setGameObject(new GameObject(GameObjectType.VILLAGER_NW));
		aSegments[33][6].setGameObject(new GameObject(GameObjectType.VILLAGER_NW));
		aSegments[33][6].setGameObject(new GameObject(GameObjectType.VILLAGER_NW));
		aSegments[45][5].setGameObject(new GameObject(GameObjectType.VILLAGER_W));
		aSegments[43][9].setGameObject(new GameObject(GameObjectType.VILLAGER_NE));

		// thief hideout

		// 3/18
		/*
		 * Random random = new Random(); for (int i = 3; i < aSegments.length;
		 * i++) { for (int j = 0; j < aSegments.length; j+=random.nextInt(13)+1)
		 * { if (j>17) { System.out.println("aSegments["+i+"]["+j+
		 * "].setGameObject(new GameObject(TREE_1));"); }
		 * 
		 * } }
		 */

		// generovane prostredie

		aSegments[3][23].setGameObject(new GameObject(TREE_1));
		aSegments[3][28].setGameObject(new GameObject(TREE_1));
		aSegments[3][35].setGameObject(new GameObject(TREE_1));
		aSegments[3][36].setGameObject(new GameObject(TREE_1));
		aSegments[3][46].setGameObject(new GameObject(TREE_1));
		aSegments[4][22].setGameObject(new GameObject(TREE_1));
		aSegments[4][32].setGameObject(new GameObject(TREE_1));
		aSegments[4][40].setGameObject(new GameObject(TREE_1));
		aSegments[4][48].setGameObject(new GameObject(TREE_1));
		aSegments[5][26].setGameObject(new GameObject(TREE_1));
		aSegments[5][32].setGameObject(new GameObject(TREE_1));
		aSegments[5][34].setGameObject(new GameObject(TREE_1));
		aSegments[5][37].setGameObject(new GameObject(TREE_1));
		aSegments[5][49].setGameObject(new GameObject(TREE_1));
		aSegments[6][18].setGameObject(new GameObject(TREE_1));
		aSegments[6][27].setGameObject(new GameObject(TREE_2));
		aSegments[6][40].setGameObject(new GameObject(TREE_1));
		aSegments[7][20].setGameObject(new GameObject(TREE_1));
		aSegments[7][22].setGameObject(new GameObject(TREE_1));
		aSegments[7][29].setGameObject(new GameObject(TREE_1));
		aSegments[7][32].setGameObject(new GameObject(TREE_1));
		aSegments[7][44].setGameObject(new GameObject(TREE_1));
		aSegments[8][25].setGameObject(new GameObject(TREE_1));
		aSegments[8][36].setGameObject(new GameObject(TREE_1));
		aSegments[8][44].setGameObject(new GameObject(TREE_1));
		aSegments[9][20].setGameObject(new GameObject(TREE_2));
		aSegments[9][21].setGameObject(new GameObject(TREE_1));
		aSegments[9][29].setGameObject(new GameObject(TREE_1));
		aSegments[9][34].setGameObject(new GameObject(TREE_1));
		aSegments[9][41].setGameObject(new GameObject(TREE_1));
		aSegments[9][48].setGameObject(new GameObject(TREE_1));
		aSegments[10][30].setGameObject(new GameObject(TREE_2));
		aSegments[10][43].setGameObject(new GameObject(TREE_1));
		aSegments[10][45].setGameObject(new GameObject(TREE_1));
		aSegments[11][22].setGameObject(new GameObject(TREE_1));
		aSegments[11][34].setGameObject(new GameObject(TREE_1));
		aSegments[11][40].setGameObject(new GameObject(TREE_1));
		aSegments[11][48].setGameObject(new GameObject(TREE_1));
		aSegments[12][25].setGameObject(new GameObject(TREE_1));
		aSegments[12][37].setGameObject(new GameObject(TREE_1));
		aSegments[12][47].setGameObject(new GameObject(TREE_1));
		aSegments[13][25].setGameObject(new GameObject(TREE_1));
		aSegments[13][31].setGameObject(new GameObject(TREE_1));
		aSegments[13][33].setGameObject(new GameObject(TREE_1));
		aSegments[13][44].setGameObject(new GameObject(TREE_1));
		aSegments[14][18].setGameObject(new GameObject(TREE_1));
		aSegments[14][27].setGameObject(new GameObject(TREE_2));
		aSegments[14][34].setGameObject(new GameObject(TREE_1));
		aSegments[14][42].setGameObject(new GameObject(TREE_1));
		aSegments[15][19].setGameObject(new GameObject(TREE_1));
		aSegments[15][29].setGameObject(new GameObject(TREE_1));
		aSegments[15][36].setGameObject(new GameObject(TREE_1));
		aSegments[15][42].setGameObject(new GameObject(TREE_1));
		aSegments[16][20].setGameObject(new GameObject(TREE_1));
		aSegments[16][30].setGameObject(new GameObject(TREE_1));
		aSegments[16][33].setGameObject(new GameObject(TREE_1));
		aSegments[16][41].setGameObject(new GameObject(TREE_1));
		aSegments[16][48].setGameObject(new GameObject(TREE_1));
		aSegments[17][18].setGameObject(new GameObject(TREE_1));
		aSegments[17][20].setGameObject(new GameObject(TREE_1));
		aSegments[17][26].setGameObject(new GameObject(TREE_1));
		aSegments[17][34].setGameObject(new GameObject(TREE_1));
		aSegments[17][44].setGameObject(new GameObject(TREE_1));
		aSegments[18][20].setGameObject(new GameObject(TREE_1));
		aSegments[18][27].setGameObject(new GameObject(TREE_1));
		aSegments[18][40].setGameObject(new GameObject(TREE_1));
		aSegments[18][47].setGameObject(new GameObject(TREE_1));
		aSegments[19][19].setGameObject(new GameObject(TREE_2));
		aSegments[19][25].setGameObject(new GameObject(TREE_1));
		aSegments[19][28].setGameObject(new GameObject(TREE_1));
		aSegments[19][39].setGameObject(new GameObject(TREE_1));
		aSegments[19][41].setGameObject(new GameObject(TREE_1));
		aSegments[19][42].setGameObject(new GameObject(TREE_1));
		aSegments[19][45].setGameObject(new GameObject(TREE_1));
		aSegments[19][47].setGameObject(new GameObject(TREE_1));
		aSegments[20][20].setGameObject(new GameObject(TREE_1));
		aSegments[20][23].setGameObject(new GameObject(TREE_1));
		aSegments[20][36].setGameObject(new GameObject(TREE_1));
		aSegments[20][48].setGameObject(new GameObject(TREE_1));
		aSegments[20][49].setGameObject(new GameObject(TREE_1));
		aSegments[21][18].setGameObject(new GameObject(TREE_1));
		aSegments[21][26].setGameObject(new GameObject(TREE_1));
		aSegments[21][33].setGameObject(new GameObject(TREE_1));
		aSegments[21][44].setGameObject(new GameObject(TREE_1));
		aSegments[22][21].setGameObject(new GameObject(TREE_1));
		aSegments[22][25].setGameObject(new GameObject(TREE_1));
		aSegments[22][26].setGameObject(new GameObject(TREE_1));
		aSegments[22][37].setGameObject(new GameObject(TREE_1));
		aSegments[22][47].setGameObject(new GameObject(TREE_1));
		aSegments[23][19].setGameObject(new GameObject(TREE_1));
		aSegments[23][25].setGameObject(new GameObject(TREE_2));
		aSegments[23][36].setGameObject(new GameObject(TREE_1));
		aSegments[23][41].setGameObject(new GameObject(TREE_1));
		aSegments[23][43].setGameObject(new GameObject(TREE_1));
		aSegments[24][18].setGameObject(new GameObject(TREE_1));
		aSegments[24][27].setGameObject(new GameObject(TREE_1));
		aSegments[24][37].setGameObject(new GameObject(TREE_1));
		aSegments[24][47].setGameObject(new GameObject(TREE_1));
		aSegments[25][21].setGameObject(new GameObject(TREE_1));
		aSegments[25][26].setGameObject(new GameObject(TREE_1));
		aSegments[25][30].setGameObject(new GameObject(TREE_1));
		aSegments[25][31].setGameObject(new GameObject(TREE_1));
		aSegments[25][38].setGameObject(new GameObject(TREE_1));
		aSegments[25][43].setGameObject(new GameObject(TREE_1));
		aSegments[25][45].setGameObject(new GameObject(TREE_1));
		aSegments[26][26].setGameObject(new GameObject(TREE_1));
		aSegments[26][31].setGameObject(new GameObject(TREE_1));
		aSegments[26][44].setGameObject(new GameObject(TREE_1));
		aSegments[26][48].setGameObject(new GameObject(TREE_1));
		aSegments[27][24].setGameObject(new GameObject(TREE_2));
		aSegments[27][29].setGameObject(new GameObject(TREE_1));
		aSegments[27][40].setGameObject(new GameObject(TREE_1));
		aSegments[28][22].setGameObject(new GameObject(TREE_1));
		aSegments[28][31].setGameObject(new GameObject(TREE_1));
		aSegments[28][39].setGameObject(new GameObject(TREE_1));
		aSegments[28][44].setGameObject(new GameObject(TREE_1));
		aSegments[28][47].setGameObject(new GameObject(TREE_1));
		aSegments[29][19].setGameObject(new GameObject(TREE_1));
		aSegments[29][27].setGameObject(new GameObject(TREE_1));
		aSegments[29][39].setGameObject(new GameObject(TREE_1));
		aSegments[29][42].setGameObject(new GameObject(TREE_1));
		aSegments[29][45].setGameObject(new GameObject(TREE_1));
		aSegments[29][49].setGameObject(new GameObject(TREE_1));
		aSegments[30][19].setGameObject(new GameObject(TREE_1));
		aSegments[30][20].setGameObject(new GameObject(TREE_1));
		aSegments[30][27].setGameObject(new GameObject(TREE_1));
		aSegments[30][37].setGameObject(new GameObject(TREE_1));
		aSegments[30][47].setGameObject(new GameObject(TREE_1));
		aSegments[31][22].setGameObject(new GameObject(TREE_1));
		aSegments[31][33].setGameObject(new GameObject(TREE_1));
		aSegments[31][34].setGameObject(new GameObject(TREE_1));
		aSegments[31][39].setGameObject(new GameObject(TREE_1));
		aSegments[31][40].setGameObject(new GameObject(TREE_2));
		aSegments[31][47].setGameObject(new GameObject(TREE_1));
		aSegments[32][26].setGameObject(new GameObject(TREE_1));
		aSegments[32][33].setGameObject(new GameObject(TREE_1));
		aSegments[32][34].setGameObject(new GameObject(TREE_1));
		aSegments[32][45].setGameObject(new GameObject(TREE_1));
		aSegments[33][19].setGameObject(new GameObject(TREE_1));
		aSegments[33][26].setGameObject(new GameObject(TREE_1));
		aSegments[33][37].setGameObject(new GameObject(TREE_1));
		aSegments[33][47].setGameObject(new GameObject(TREE_1));
		aSegments[34][21].setGameObject(new GameObject(TREE_1));
		aSegments[34][29].setGameObject(new GameObject(TREE_1));
		aSegments[34][42].setGameObject(new GameObject(TREE_1));
		aSegments[34][46].setGameObject(new GameObject(TREE_1));
		aSegments[34][47].setGameObject(new GameObject(TREE_1));
		aSegments[35][23].setGameObject(new GameObject(TREE_1));
		aSegments[35][35].setGameObject(new GameObject(TREE_1));
		aSegments[35][38].setGameObject(new GameObject(TREE_1));
		aSegments[35][40].setGameObject(new GameObject(TREE_1));
		aSegments[35][42].setGameObject(new GameObject(TREE_1));
		aSegments[35][45].setGameObject(new GameObject(TREE_1));
		aSegments[36][19].setGameObject(new GameObject(TREE_1));
		aSegments[36][31].setGameObject(new GameObject(TREE_1));
		aSegments[36][34].setGameObject(new GameObject(TREE_1));
		aSegments[36][40].setGameObject(new GameObject(TREE_1));
		aSegments[36][44].setGameObject(new GameObject(TREE_2));
		aSegments[37][26].setGameObject(new GameObject(TREE_1));
		aSegments[37][39].setGameObject(new GameObject(TREE_1));
		aSegments[38][30].setGameObject(new GameObject(TREE_1));
		aSegments[38][35].setGameObject(new GameObject(TREE_1));
		aSegments[38][36].setGameObject(new GameObject(TREE_1));
		aSegments[38][48].setGameObject(new GameObject(TREE_1));
		aSegments[38][49].setGameObject(new GameObject(TREE_1));
		aSegments[39][22].setGameObject(new GameObject(TREE_1));
		aSegments[39][30].setGameObject(new GameObject(TREE_1));
		aSegments[39][33].setGameObject(new GameObject(TREE_1));
		aSegments[39][46].setGameObject(new GameObject(TREE_1));
		aSegments[39][48].setGameObject(new GameObject(TREE_1));
		aSegments[40][28].setGameObject(new GameObject(TREE_1));
		aSegments[40][30].setGameObject(new GameObject(TREE_1));
		aSegments[40][39].setGameObject(new GameObject(TREE_1));
		aSegments[40][49].setGameObject(new GameObject(TREE_1));
		aSegments[41][20].setGameObject(new GameObject(TREE_1));
		aSegments[41][23].setGameObject(new GameObject(TREE_1));
		aSegments[41][31].setGameObject(new GameObject(TREE_1));
		aSegments[41][38].setGameObject(new GameObject(TREE_1));
		aSegments[41][43].setGameObject(new GameObject(TREE_1));
		aSegments[42][23].setGameObject(new GameObject(TREE_1));
		aSegments[42][33].setGameObject(new GameObject(TREE_1));
		aSegments[42][40].setGameObject(new GameObject(TREE_1));
		aSegments[42][49].setGameObject(new GameObject(TREE_1));
		aSegments[43][18].setGameObject(new GameObject(TREE_1));
		aSegments[43][24].setGameObject(new GameObject(TREE_2));
		aSegments[43][32].setGameObject(new GameObject(TREE_1));
		aSegments[43][44].setGameObject(new GameObject(TREE_1));
		aSegments[44][21].setGameObject(new GameObject(TREE_1));
		aSegments[44][30].setGameObject(new GameObject(TREE_1));
		aSegments[44][32].setGameObject(new GameObject(TREE_1));
		aSegments[44][39].setGameObject(new GameObject(TREE_1));
		aSegments[45][22].setGameObject(new GameObject(TREE_1));
		aSegments[45][25].setGameObject(new GameObject(TREE_1));
		aSegments[45][27].setGameObject(new GameObject(TREE_1));
		aSegments[45][32].setGameObject(new GameObject(TREE_1));
		aSegments[45][38].setGameObject(new GameObject(TREE_1));
		aSegments[45][41].setGameObject(new GameObject(TREE_1));
		aSegments[45][48].setGameObject(new GameObject(TREE_1));
		aSegments[46][25].setGameObject(new GameObject(TREE_1));
		aSegments[46][46].setGameObject(new GameObject(TREE_1));
		aSegments[47][19].setGameObject(new GameObject(TREE_1));
		aSegments[47][28].setGameObject(new GameObject(TREE_1));
		aSegments[47][32].setGameObject(new GameObject(TREE_1));
		aSegments[47][44].setGameObject(new GameObject(TREE_1));
		aSegments[48][18].setGameObject(new GameObject(TREE_1));
		aSegments[48][23].setGameObject(new GameObject(TREE_1));
		aSegments[48][33].setGameObject(new GameObject(TREE_1));
		aSegments[48][34].setGameObject(new GameObject(TREE_1));
		aSegments[48][47].setGameObject(new GameObject(TREE_1));
		aSegments[49][25].setGameObject(new GameObject(TREE_1));
		aSegments[49][29].setGameObject(new GameObject(TREE_1));
		aSegments[49][37].setGameObject(new GameObject(TREE_1));
		aSegments[49][47].setGameObject(new GameObject(TREE_2));

	}

	public void addNPC(Unit unit) {
		units.add(unit);
		
	}

	public boolean isVillageDestroyed() {
		return isVillageDestroyed;
	}

	public void setVillageDestroyed() {
		
		aSegments[39][11].setGameObject(new GameObject(VILLAGER_DEAD));
		aSegments[33][6].setGameObject(new GameObject(VILLAGER_DEAD));
		aSegments[33][6].setGameObject(new GameObject(VILLAGER_DEAD));
		aSegments[33][6].setGameObject(new GameObject(VILLAGER_DEAD));
		aSegments[45][5].setGameObject(new GameObject(VILLAGER_DEAD));
		aSegments[43][9].setGameObject(new GameObject(VILLAGER_DEAD));
		FriendlyNPC npc = (FriendlyNPC) aSegments[39][2].getGameObject();
		npc.takeDamage(10000);
		aSegments[38][2].setGameObject(new GameObject(VILLAGER_DEAD));
		this.isVillageDestroyed = true;
	}

}