package com.matusmahut.isometricRPG.map;

import static org.lwjgl.opengl.GL11.*;

import java.awt.Point;

import org.lwjgl.opengl.GL11;

import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.graphics.Font;
import com.matusmahut.isometricRPG.math.Vector3f;

public class Segment {
	private int aX;
	private int aY;
	private int aSize;
	private boolean isIso = true;
	private Point aPointTop = new Point();
	private Point aPointRight = new Point();
	private Point aPointLeft = new Point();
	private Point aPointBot = new Point();
	private Vector3f aColor;
	private boolean isFilled = false;
	private GameObject aGameObject;
	private GameObject aSurface = new GameObject(GameObjectType.S_GRASS);
	private GameObject aSurfaceH = new GameObject(GameObjectType.S_GRASS_HIGHLIGHTED);
	private boolean isBlocked = false;
	private int aCost;
	private int aDistance;
	private Segment aParent;
	private Font font;
	private boolean isHighLighted;

	public Segment(int paX, int paY, int paSize) {
		aX = paX;
		aY = paY;
		aSize = paSize;
		aColor = new Vector3f(1f, 1f, 1f);
		aGameObject = null;
		calculatePoints();
		aSurface.setPosition(this);
		aSurfaceH.setPosition(this);
	}

	private void calculatePoints() {

		aPointTop.setLocation(aSize * aX, aSize * aY);
		aPointTop = calculatePoint(aPointTop);
		aPointRight.setLocation(aSize * aX + aSize, aSize * aY);
		aPointRight = calculatePoint(aPointRight);
		aPointLeft.setLocation(aSize * aX, aSize * aY + aSize);
		aPointLeft = calculatePoint(aPointLeft);
		aPointBot.setLocation(aSize * aX + aSize, aSize * aY + aSize);
		aPointBot = calculatePoint(aPointBot);

	}

	public void render() {

		if (aGameObject != null) {
			aGameObject.render();
		}

		/*
		  if (isFilled) renderGridQ(); else renderGridL();
		  
		  renderText();
		 */
	}

	public void renderText() {
		if (font != null) {
			font.drawText(aX + "," + aY, aPointLeft.x + 40, aPointLeft.y - 5);
		}
	}

	public void renderSurface() {
		
		
		if (isHighLighted) {
			aSurfaceH.render();
		} else
			aSurface.render();
	}

	public void setGameObject(GameObject gObject) {
		aGameObject = gObject;
		aGameObject.setPosition(this);
		setBlocked(true);
	}

	public void setPlayer(GameObject gObject) {
		aGameObject = gObject;
		aGameObject.setaSegment(this);
		setBlocked(true);
	}

	public void renderGridQ() {

		glColor4f(aColor.getX(), aColor.getY(), aColor.getZ(), 0.3f);
		glBegin(GL11.GL_QUADS);
		glVertex2f(aPointTop.x, aPointTop.y);
		glVertex2f(aPointRight.x, aPointRight.y);
		glVertex2f(aPointBot.x, aPointBot.y);
		glVertex2f(aPointLeft.x, aPointLeft.y);
		glEnd();
	}

	public void renderGridL() {

		glColor3f(0f, 0f, 0f);

		glBegin(GL11.GL_LINES);
		glVertex2f(aPointTop.x, aPointTop.y);
		glVertex2f(aPointRight.x, aPointRight.y);
		glEnd();

		glBegin(GL11.GL_LINES);
		glVertex2f(aPointTop.x, aPointTop.y);
		glVertex2f(aPointLeft.x, aPointLeft.y);
		glEnd();

		glBegin(GL11.GL_LINES);
		glVertex2f(aPointRight.x, aPointRight.y);
		glVertex2f(aPointBot.x, aPointBot.y);
		glEnd();

		glBegin(GL11.GL_LINES);
		glVertex2f(aPointLeft.x, aPointLeft.y);
		glVertex2f(aPointBot.x, aPointBot.y);
		glEnd();
	}

	private Point calculatePoint(Point point) {
		if (isIso) {
			return fromCartToIso(point);
		} else
			return point;
	}

	private Point fromCartToIso(Point point) {
		int x = point.x - point.y;
		int y = (point.x + point.y) / 2;
		return new Point(x, y);
	}

	public void changeColor(Vector3f color) {
		aColor = color;
	}

	public Point getPoint() {
		return aPointLeft;
	}

	public void setFilled(boolean isFilled) {
		this.isFilled = isFilled;
	}

	public boolean isBlocked() {
		return isBlocked;
	}

	public void setBlocked(boolean isBlocked) {
		this.isBlocked = isBlocked;
	}

	public int getXIndex() {
		return aX;
	}

	public int getYIndex() {
		return aY;
	}

	public int getF() {
		return aCost + aDistance;
	}

	public void setCost(int cost) {
		aCost = cost;
	}

	public int getCost() {
		return aCost;
	}

	public void setDistance(int distance) {
		aDistance = distance;
	}

	public void setParent(Segment parent) {
		aParent = parent;
	}

	public Segment getParent() {
		return aParent;
	}

	public void dispose() {
		aGameObject = null;
		isBlocked = false;
	}

	public GameObject getGameObject() {

		return aGameObject;
	}

	public void setFont(Font font) {
		this.font = font;

	}

	public boolean isHighLighted() {
		return isHighLighted;
	}

	public void setHighLighted(boolean isHighLighted) {
		this.isHighLighted = isHighLighted;
	}

}