package com.matusmahut.isometricRPG.gameObject;

import com.matusmahut.isometricRPG.graphics.Texture;
import com.matusmahut.isometricRPG.graphics.VertexArray;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.math.*;

public class GameObject {

	protected Vector3f aPosition;
	protected Vector2f aSize;
	private Texture aTexture;
	private VertexArray aMesh;
	protected GameObjectType aType;
	private boolean initialized;
	protected Segment aSegment;
	private float height = 600;

	public GameObject(Vector3f position, GameObjectType type) {
		aPosition = position;
		aTexture = type.getTexture();
		aType = type;
		aSize = aType.getSize();
		init();
	}

	public GameObject(GameObjectType type) {
		aPosition = new Vector3f();
		aTexture = type.getTexture();
		aType = type;
		aSize = aType.getSize();
	}
	

	private void init() {
		float[] vertices = new float[] {

				aPosition.x + aType.getMargin().x, -height - aPosition.y + aType.getMargin().y, aPosition.z,
				
				aPosition.x + aType.getMargin().x, -height - aPosition.y - aSize.y + aType.getMargin().y,
				aPosition.z,
				
				aPosition.x + aSize.x + aType.getMargin().x,
				-height - aPosition.y - aSize.y + aType.getMargin().y, aPosition.z,
				
				aPosition.x +aSize.x + aType.getMargin().x, -height - aPosition.y + aType.getMargin().y,
				aPosition.z };

		byte[] indices = new byte[] {
				0, 1, 2,
				2, 3, 0
			};
			
		float[] tcs = new float[] {
				0, 1,
				0, 0,
				1, 0,
				1, 1
			};

		aMesh = new VertexArray(vertices, indices, tcs);
		initialized = true;
	}

	public void render() {
		if (initialized) {			
			aType.getShader().enable();
			aTexture.bind();
			aMesh.render();
			aType.getShader().disable();			
		}
	}	

	public void setPosition(Segment segment) {
		aSegment=segment;
		aPosition = new Vector3f(segment.getPoint().x, -segment.getPoint().y, 0);
		init();
	}
	
	public void setGameObject(float x,float y, float sizeX, float sizeY, float marginX, float marginY){
		aPosition = new Vector3f(x, y, 0);
		aSize = new Vector2f(sizeX,sizeY);
		init();
	}

	public Segment getaSegment() {
		return aSegment;
	}

	public void setaSegment(Segment aSegment) {
		this.aSegment = aSegment;
	}
	
	public void setTexture(Texture texture) {
		aTexture = texture;
	}
	
	public void setZ(float z) {
		aPosition.z=z;
		init();
	}
	
	

}
