package com.matusmahut.isometricRPG.gameObject;

import static com.matusmahut.isometricRPG.engine.Game.*;
import com.matusmahut.isometricRPG.graphics.Shader;
import com.matusmahut.isometricRPG.graphics.Texture;
import com.matusmahut.isometricRPG.math.Vector2f;

public enum GameObjectType {
	//NAZOV(sirka, vyska, posunX, posunY, zdroj, shader)

	//environment objects
	TREE_1(200, 200, -75, 35, "resources/treeI1.png", Shader.STATIC),
	TREE_2(175, 175, -65, 30, "resources/treeL1.png", Shader.STATIC),
	TREE_3(200, 200, -75, 35, "resources/treeL2.png", Shader.STATIC),
	PAVILION(100, 100, 0, 25, "resources/pavilion.png", Shader.STATIC),
	OUTPOST_SW(180, 180, -50, 25, "resources/Outpost/old_outpost_iso_80000.png", Shader.STATIC),
	OUTPOST_SE(180, 180, -50, 25, "resources/Outpost/old_outpost_iso_120000.png", Shader.STATIC),
	OUTPOST_NE(180, 180, -50, 25, "resources/Outpost/old_outpost_iso_00000.png", Shader.STATIC),
	OUTPOST_NW(180, 180, -50, 25, "resources/Outpost/old_outpost_iso_40000.png", Shader.STATIC),
	WELL_SW(80, 80, 5, 15, "resources/Well/Well_00000.png", Shader.STATIC),
	WELL_SE(80, 80, 5, 15, "resources/Well/Well_60000.png", Shader.STATIC),
	TENT1(100, 100, 0, 25, "resources/tent1.png", Shader.STATIC),
	TENT2(100, 100, 0, 25, "resources/tent2.png", Shader.STATIC),
	TIMBER1(100, 100, 0, 25, "resources/timber1.png", Shader.STATIC),
	TIMBER2(100, 100, 0, 25, "resources/timber2.png", Shader.STATIC),
	CRATES1(100, 200, 0, 40, "resources/IsoCollection/woodenCrates_N.png", Shader.STATIC),
	BARRELS1(75, 150, 15, 35, "resources/IsoCollection/barrels_N.png", Shader.STATIC),
	TABLE2(100, 200, 0, 40, "resources/IsoCollection/tableShortChairs_N.png", Shader.STATIC),
	//units
	PLAYER(140,140,-20,35,"resources/warrior.png",Shader.RELATIVE),
	HERO(140,140,-20,35,"resources/warrior.png",Shader.STATIC),
	SWORDSMAN(80,80,-10,15,"resources/warrior.png",Shader.STATIC),
	MINOTAUR(120,120,-10,35,"resources/Minotaur/Idle/minotaur_alpha [www.imagesplitter.net]-0-0.png",Shader.STATIC),
	ZOMBIE(150,150,-23,40,"resources/Minotaur/Idle/minotaur_alpha [www.imagesplitter.net]-0-0.png",Shader.STATIC),
	VILLAGER_DEAD(100,85,0,25,"resources/Villager_01/Villager_01/Death/villager_death_10015.png",Shader.STATIC),
	VILLAGER_S(100,85,0,25,"resources/Villager_01/Villager_01/Ilde/villager_idle_00000.png",Shader.STATIC),
	VILLAGER_SE(100,85,0,25,"resources/Villager_01/Villager_01/Ilde/villager_idle_10000.png",Shader.STATIC),
	VILLAGER_E(100,85,0,25,"resources/Villager_01/Villager_01/Ilde/villager_idle_20000.png",Shader.STATIC),
	VILLAGER_NE(100,85,0,25,"resources/Villager_01/Villager_01/Ilde/villager_idle_30000.png",Shader.STATIC),
	VILLAGER_N(100,85,0,25,"resources/Villager_01/Villager_01/Ilde/villager_idle_40000.png",Shader.STATIC),
	VILLAGER_NW(100,85,0,25,"resources/Villager_01/Villager_01/Ilde/villager_idle_50000.png",Shader.STATIC),
	VILLAGER_W(100,85,0,25,"resources/Villager_01/Villager_01/Ilde/villager_idle_60000.png",Shader.STATIC),
	VILLAGER_SW(100,85,0,25,"resources/Villager_01/Villager_01/Ilde/villager_idle_70000.png",Shader.STATIC),
	QUINN(38,75,30,10,"resources/quinn.png",Shader.STATIC),
	ALICE_DEAD(140,140,-20,35,"resources/Alice/Dead/alice_dead.png",Shader.STATIC),
	//loot
	CHEST_CLOSED_1(160,320,-30,60,"resources/IsoCollection/chestClosed_N.png",Shader.STATIC),
	CHEST_OPENED_1(160,320,-30,60,"resources/IsoCollection/chestOpen_N.png",Shader.STATIC),		
	
	//surface
	S_GRASS(100,50,0,25,"resources/isograss.png",Shader.STATIC),	
	S_GRASS_HIGHLIGHTED(100,50,0,25,"resources/isograssH.png",Shader.STATIC),	
	
	//interface 
	UI_LOOT(sWidth/4,sHeight/5,0,0,"resources/RPG Inventory/lootmenu.png",Shader.RELATIVE),
	UI_INVENTORY(sWidth/2,sHeight,0,0,"resources/RPG Inventory/inventory.png",Shader.RELATIVE),
	UI_LEVELPANEL(sWidth/2,sHeight,0,0,"resources/RPG Inventory/levelMenu.png",Shader.RELATIVE),
	UI_MENU(sWidth,sHeight,0,0,"resources/RPG Inventory/menu.png",Shader.RELATIVE),
	UI_CREDITS(sWidth,sHeight,0,0,"resources/RPG Inventory/credits.png",Shader.RELATIVE),
	UI_CONTROLS(sWidth,sHeight,0,0,"resources/RPG Inventory/controls.png",Shader.RELATIVE),
	UI_GAMEOVER(sWidth/2,sHeight/2,0,0,"resources/RPG Inventory/gameOver.png",Shader.RELATIVE),
	UI_DIALOG(sWidth/5,sHeight/7,-120,-70,"resources/Comic/Comic/comic-2.png",Shader.RELATIVE),
	UI_DIALOG_OPTIONS(sWidth/5,sHeight/7,-120,-100,"resources/RPG Inventory/choicesMenu.png",Shader.RELATIVE),
	
	//icons
	ICON_HEALTH(sWidth/2,sHeight,0,0,"resources/abilityIcons/health.png",Shader.RELATIVE),
	ICON_DAMAGE(sWidth/2,sHeight,0,0,"resources/abilityIcons/damage.png",Shader.RELATIVE),
	ICON_TOXICITY(sWidth/2,sHeight,0,0,"resources/abilityIcons/toxicity.png",Shader.RELATIVE),
	ICON_ARMOR(sWidth/2,sHeight,0,0,"resources/abilityIcons/armor.png",Shader.RELATIVE),
	ICON_AGILITY(0,0,0,0,"resources/abilityIcons/agility.png",Shader.RELATIVE),
	ICON_STRENGTH(0,0,0,0,"resources/abilityIcons/strength.png",Shader.RELATIVE),
	ICON_INTELIGENCE(0,0,0,0,"resources/abilityIcons/inteligence.png",Shader.RELATIVE),
	ICON_VITALITY(0,0,0,0,"resources/abilityIcons/vitality.png",Shader.RELATIVE),
	
	
	// inventory objects
	I_POTION_DAMAGE(0,0,0,0,"resources/InventoryIcons/potions/pt4.png", Shader.RELATIVE),
	I_POTION_HEALTH(0,0,0,0,"resources/InventoryIcons/potions/pt1.png", Shader.RELATIVE),
	I_ARMOR_CLOTH(0,0,0,0,"resources/InventoryIcons/armor/armor_padded.png", Shader.RELATIVE),
	I_ARMOR_LEATHER(0,0,0,0,"resources/InventoryIcons/armor/armor_leather.png", Shader.RELATIVE),
	I_ARMOR_CHAINMAIL(0,0,0,0,"resources/InventoryIcons/armor/armor_chainmail.png", Shader.RELATIVE),
	I_ARMOR_PLATE(0,0,0,0,"resources/InventoryIcons/armor/armor_plate.png", Shader.RELATIVE),
	I_WEAPON_DAGGER(0,0,0,0,"resources/InventoryIcons/weapons/dagger.png", Shader.RELATIVE),
	I_WEAPON_SHORTSWORD(0,0,0,0,"resources/InventoryIcons/weapons/shortsword.png", Shader.RELATIVE),
	I_WEAPON_LONGSWORD(0,0,0,0,"resources/InventoryIcons/weapons/longsword.png", Shader.RELATIVE),
	I_WEAPON_GREATSWORD(0,0,0,0,"resources/InventoryIcons/weapons/greatsword.png", Shader.RELATIVE),
	I_WEAPON_VIPER(0,0,0,0,"resources/InventoryIcons/weapons/viper.png", Shader.RELATIVE);
	

	private final Vector2f size;
	private final Vector2f margin;
	private final Texture texture;
	private final Shader shader;

	private GameObjectType(float width, float height, float marginX, float marginY, String texturePath, Shader shader) {
		this.size = new Vector2f(width, height);
		this.margin = new Vector2f(marginX, marginY);
		this.texture = new Texture(texturePath);
		this.shader = shader;
	}

	public Vector2f getSize() {
		return size;
	}
	
	public Vector2f getMargin() {
		return margin;
	}
	
	public Texture getTexture() {
		return texture;
	}
	
	public Shader getShader() {
		return shader;
	}

}
