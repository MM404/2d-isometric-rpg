package com.matusmahut.isometricRPG.gameObject;

import java.util.ArrayList;

import com.matusmahut.isometricRPG.gameObject.inventoryObject.InventoryObject;
import com.matusmahut.isometricRPG.gameObject.units.Player;
import com.matusmahut.isometricRPG.map.Segment;

public class LootObject extends GameObject implements Interactable {

	private Segment lootSegment;
	public ArrayList<InventoryObject> objects = new ArrayList<InventoryObject>();

	public LootObject(GameObjectType type) {
		super(type);
	}

	public LootObject(GameObjectType type, Segment segment) {
		super(type);
		lootSegment=segment;
	}

	public void interact(Player player) {

		//ak je hr�� pri objekte rovno lootuje ak nie tak sa mu nastav� cesta k objektu
		if (player.getaSegment() == lootSegment) {
			player.loot(this);
		} else
			player.setPath(lootSegment, this);

	}
	

	public void addLoot(InventoryObject loot){
		this.objects.add(loot);
	}
	
	public ArrayList<InventoryObject> getInventoryObjects(){
		return objects;
	}

	/*public void empty() {
		this.setTexture(GameObjectType.CHEST_OPENED_1.getTexture());
		
	}*/

	public void remove(InventoryObject object) {
		objects.remove(object);
		
	}
	
	public Segment getLootSegment(){
		return lootSegment;
	}

}
