package com.matusmahut.isometricRPG.gameObject;

import com.matusmahut.isometricRPG.gameObject.units.Player;

public interface Interactable {

	public void interact(Player player);
	
}
