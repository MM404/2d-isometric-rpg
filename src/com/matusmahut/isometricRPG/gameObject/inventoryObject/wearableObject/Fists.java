package com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject;

import com.matusmahut.isometricRPG.gameObject.GameObjectType;

public class Fists extends Weapon {

	public Fists() {
		super(GameObjectType.I_POTION_DAMAGE, "Fists", 4, 400);
	}

}
