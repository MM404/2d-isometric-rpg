package com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject;

import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.InventoryObject;
import com.matusmahut.isometricRPG.gameObject.units.Player;

public abstract class WearableInventoryObject extends InventoryObject {

	private boolean isActive;
	
	public WearableInventoryObject(GameObjectType type, String name) {
		super(type, name);
		// TODO Auto-generated constructor stub
	}

	protected void wear(Player player) {
		// TODO Auto-generated method stub
		
	}

	protected void unwear(Player player) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void use(Player player) {
		if (isActive) {
			unwear(player);
			isActive=false;
		} else{
			wear(player);
			isActive=true;
		}
		
	}
	
	public boolean isActive(){
		return isActive;
	}

}
