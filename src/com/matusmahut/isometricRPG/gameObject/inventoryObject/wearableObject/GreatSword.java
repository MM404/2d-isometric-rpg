package com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject;

import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.units.Player;

public class GreatSword extends Weapon {

	public GreatSword() {
		super(GameObjectType.I_WEAPON_GREATSWORD, "Great Sword", 55, 1050);
	}

	protected void wear(Player player) {
		player.addAnimation(AnimationType.HERO_WEAPON_GREATSWORD);
		super.wear(player);
	}

	protected void unwear(Player player) {
		player.removeAnimation(AnimationType.HERO_WEAPON_GREATSWORD);
		super.unwear(player);
	}
}
