package com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject;

import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.units.Player;

public class LongSword extends Weapon {

	public LongSword() {
		super(GameObjectType.I_WEAPON_LONGSWORD,"Long Sword",30,800);
	}
	
	protected void wear(Player player){
		player.addAnimation(AnimationType.HERO_WEAPON_LONGSWORD);
		super.wear(player);
	}
	protected void unwear(Player player){
		player.removeAnimation(AnimationType.HERO_WEAPON_LONGSWORD);
		super.unwear(player);
	}
}
