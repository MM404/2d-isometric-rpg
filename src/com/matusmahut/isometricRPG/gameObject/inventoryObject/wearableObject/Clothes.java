package com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject;

import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.units.Player;

public class Clothes extends Armor {

	public Clothes() {
		super(GameObjectType.I_POTION_DAMAGE,"Clothes",0,0);
		// TODO Auto-generated constructor stub
	}

	protected void wear(Player player) {
		player.addAnimation(AnimationType.HERO_CLOTHES);
		super.wear(player);
	}

	protected void unwear(Player player) {
		player.removeAnimation(AnimationType.HERO_CLOTHES);
		super.unwear(player);
	}
	
}
