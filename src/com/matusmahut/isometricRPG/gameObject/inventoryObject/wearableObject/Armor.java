package com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject;

import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.units.Player;

public abstract class Armor extends WearableInventoryObject {

	protected int armor;
	protected int attackSpeedReduction;

	public Armor(GameObjectType type, String name, int armor, int attackSpeedReduction) {
		super(type, name);
		this.armor = armor;
		this.attackSpeedReduction = attackSpeedReduction;
	}

	protected void wear(Player player) {
		player.setArmor(armor);
		player.setAttackSpeed(player.getAttackSpeed() - attackSpeedReduction);
	}

	protected void unwear(Player player) {
		player.setAttackSpeed(player.getAttackSpeed() + attackSpeedReduction);
	}
	
	public int getArmor(){
		return this.armor;
	}
}
