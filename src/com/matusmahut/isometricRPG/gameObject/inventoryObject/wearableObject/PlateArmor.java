package com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject;

import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.units.Player;

public class PlateArmor extends Armor {

	public PlateArmor() {
		super(GameObjectType.I_ARMOR_PLATE,"Plate Armor", 60, 65);
	}

	protected void wear(Player player) {
		player.addAnimation(AnimationType.HERO_ARMOR_STEEL);
		super.wear(player);
	}

	protected void unwear(Player player) {
		player.removeAnimation(AnimationType.HERO_ARMOR_STEEL);
		super.unwear(player);
	}

}