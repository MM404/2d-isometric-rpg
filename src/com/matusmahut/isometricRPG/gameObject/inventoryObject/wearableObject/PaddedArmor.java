package com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject;

import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.units.Player;

public class PaddedArmor extends Armor {

	public PaddedArmor() {
		super(GameObjectType.I_ARMOR_CLOTH,"Padded Armor", 10, 5);
		// TODO Auto-generated constructor stub
	}

	protected void wear(Player player) {
		player.addAnimation(AnimationType.HERO_ARMOR_LEATHER);
		super.wear(player);
	}

	protected void unwear(Player player) {
		player.removeAnimation(AnimationType.HERO_ARMOR_LEATHER);
		super.unwear(player);
	}

}
