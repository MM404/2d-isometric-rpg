package com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject;

import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.units.Player;

public abstract class Weapon extends WearableInventoryObject {

	protected int damage;
	protected long baseAttackTime;

	public Weapon(GameObjectType type, String name, int damage, long baseAttackTime) {
		super(type, name);
		this.damage = damage;
		this.baseAttackTime = baseAttackTime;
	}

	protected void wear(Player player) {
		player.setDamage(player.getBaseDamage() + damage);
		player.setBat(baseAttackTime);
	}

	protected void unwear(Player player) {
		player.setDamage(player.getBaseDamage() - damage);

	}

	public int getDamage() {
		return damage;
	}

	public long getBaseAttackTime() {
		return baseAttackTime;
	}

	
}
