package com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject;

import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.units.Player;

public class Dagger extends Weapon {

	public Dagger() {
		super(GameObjectType.I_WEAPON_DAGGER, "Dagger", 12, 450);
		// TODO Auto-generated constructor stub
	}

	protected void wear(Player player) {
		player.addAnimation(AnimationType.HERO_WEAPON_DAGGER);
		super.wear(player);
	}

	protected void unwear(Player player) {
		player.removeAnimation(AnimationType.HERO_WEAPON_DAGGER);
		super.unwear(player);
	}
}
