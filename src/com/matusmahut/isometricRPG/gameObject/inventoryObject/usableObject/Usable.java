package com.matusmahut.isometricRPG.gameObject.inventoryObject.usableObject;

import com.matusmahut.isometricRPG.gameObject.units.Player;

public interface Usable {
	public void use(Player player);
	
}
