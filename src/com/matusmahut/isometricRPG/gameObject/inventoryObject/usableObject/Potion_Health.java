package com.matusmahut.isometricRPG.gameObject.inventoryObject.usableObject;

import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.InventoryObject;
import com.matusmahut.isometricRPG.gameObject.units.Player;

public class Potion_Health extends InventoryObject {

	public Potion_Health() {
		super(GameObjectType.I_POTION_HEALTH,"Health Potion");
		// TODO Auto-generated constructor stub
	}

	public void use(Player player) {
		player.heal(50);		
	}

}
