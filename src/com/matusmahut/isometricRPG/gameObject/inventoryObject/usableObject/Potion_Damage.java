package com.matusmahut.isometricRPG.gameObject.inventoryObject.usableObject;

import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.InventoryObject;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.buff.DamageBuff;
import com.matusmahut.isometricRPG.gameObject.units.Player;

public class Potion_Damage extends InventoryObject {

	public Potion_Damage() {
		super(GameObjectType.I_POTION_DAMAGE,"Damage Potion");
		// TODO Auto-generated constructor stub
	}

	public void use(Player player) {
		player.addBuff(new DamageBuff(player, 5));		
	}

}
