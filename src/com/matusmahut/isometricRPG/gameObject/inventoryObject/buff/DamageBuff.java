package com.matusmahut.isometricRPG.gameObject.inventoryObject.buff;

import com.matusmahut.isometricRPG.gameObject.units.Player;

public class DamageBuff extends AbstractBuff {
	private int bonusDamage;

	public DamageBuff(Player player, int bonusDamage) {
		super(player);
		this.bonusDamage = bonusDamage;
	}

	public void start() {
		setStart(System.currentTimeMillis());
		this.getPlayer().setDamage(this.getPlayer().getDamage() + bonusDamage);
	}

	public void stop() {
		this.getPlayer().setDamage(this.getPlayer().getDamage() - bonusDamage);
		this.setEnded(true);
	}

	public void update() {
		if (System.currentTimeMillis() - this.getStart() > 0.5*60000) {
			this.stop();
		}

	}

}
