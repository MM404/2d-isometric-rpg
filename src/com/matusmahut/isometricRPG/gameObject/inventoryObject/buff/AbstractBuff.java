package com.matusmahut.isometricRPG.gameObject.inventoryObject.buff;

import com.matusmahut.isometricRPG.gameObject.units.Player;

public abstract class AbstractBuff implements Buff {

	private long start;
	private Player player;
	private boolean ended;

	public AbstractBuff(Player player) {
		this.player = player;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public Player getPlayer() {
		return player;
	}

	public boolean ended() {
		return ended;
	}

	public void setEnded(boolean ended) {
		this.ended = ended;
	}

}
