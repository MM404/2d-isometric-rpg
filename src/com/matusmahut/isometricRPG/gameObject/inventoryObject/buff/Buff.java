package com.matusmahut.isometricRPG.gameObject.inventoryObject.buff;

public interface Buff {

	public void start();
	
	public void stop();
	
	public void update();
	
}
