package com.matusmahut.isometricRPG.gameObject.inventoryObject;

import static org.lwjgl.opengl.GL11.*;

import com.matusmahut.isometricRPG.engine.Camera;
import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.usableObject.Usable;
import com.matusmahut.isometricRPG.math.Vector3f;

public abstract class InventoryObject extends GameObject implements Usable {

	private boolean highLighted;
	private String name;

	public InventoryObject(GameObjectType type, String name) {
		super(type);
		this.name = name;
	}

	public void highLight() {
		highLighted = !highLighted;
	}

	public void render() {
		//ak je objekt "kliknut�" tak nakresli� ohrani�enie
		if (highLighted) {
			glColor3f(0f, 1f, 0f);
			glLineWidth(10f);
			Vector3f cameraPos = Camera.getPosition();
			
			glBegin(GL_LINES);

			glVertex2f(this.aPosition.x - cameraPos.x, -this.aPosition.y - cameraPos.y);
			glVertex2f(this.aPosition.x - cameraPos.x, -this.aPosition.y - this.aSize.y - cameraPos.y);
			glEnd();

			glBegin(GL_LINES);
			glVertex2f(this.aPosition.x - cameraPos.x, -this.aPosition.y - cameraPos.y);
			glVertex2f(this.aPosition.x + this.aSize.x - cameraPos.x, -this.aPosition.y - cameraPos.y);
			glEnd();

			glBegin(GL_LINES);
			glVertex2f(this.aPosition.x - cameraPos.x + this.aSize.x, -this.aPosition.y - cameraPos.y);
			glVertex2f(this.aPosition.x + this.aSize.x - cameraPos.x, -this.aPosition.y - this.aSize.y - cameraPos.y);
			glEnd();

			glBegin(GL_LINES);
			glVertex2f(this.aPosition.x - cameraPos.x, -this.aPosition.y - this.aSize.y - cameraPos.y);
			glVertex2f(this.aPosition.x - cameraPos.x + this.aSize.x, -this.aPosition.y - this.aSize.y - cameraPos.y);
			glEnd();
			glLineWidth(1f);
		}
		super.render();
	}

	public String getName() {
		return name;
	}

}
