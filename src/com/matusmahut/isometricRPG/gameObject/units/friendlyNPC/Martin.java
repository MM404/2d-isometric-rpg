package com.matusmahut.isometricRPG.gameObject.units.friendlyNPC;

import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.map.Map;

public class Martin extends FriendlyNPC{

	public Martin(Map map) {
		super(GameObjectType.HERO, map);
		addAnimation(AnimationType.BANDIT);
		
	}

}
