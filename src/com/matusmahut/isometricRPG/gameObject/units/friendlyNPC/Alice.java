package com.matusmahut.isometricRPG.gameObject.units.friendlyNPC;

import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.map.Map;

public class Alice extends FriendlyNPC {

	public Alice(Map map) {
		super(GameObjectType.HERO, map);
		addAnimation(AnimationType.ALICE);
	}

}
