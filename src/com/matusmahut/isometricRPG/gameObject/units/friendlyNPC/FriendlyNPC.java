package com.matusmahut.isometricRPG.gameObject.units.friendlyNPC;


import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.gameObject.units.Player;
import com.matusmahut.isometricRPG.map.Map;
import com.matusmahut.isometricRPG.map.Segment;

public class FriendlyNPC extends NPC {

	public FriendlyNPC(GameObjectType type, Map map) {
		super(type, map);
	}

	@Override
	public void interact(Player player) {
		player.interact(this);
		
	}

	public void go(Segment segment) {
		setPath(segment);
		
	}
	
	/*public void move(){
		super.move();
	}*/

}
