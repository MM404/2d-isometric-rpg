package com.matusmahut.isometricRPG.gameObject.units;

import java.awt.Point;
import java.util.ArrayList;
import java.util.LinkedList;

import com.matusmahut.isometricRPG.animation.AnimationAction;
import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.animation.Direction;
import com.matusmahut.isometricRPG.animation.Animation;
import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.map.Map;
import com.matusmahut.isometricRPG.map.PathFinder;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.math.Vector2f;

public abstract class Unit extends GameObject {

	private int damage = 10;
	protected int health = 100;
	private int maxHealth = 100;
	private int regenPerSec = 0;
	private int vitality = 0;
	private int inteligence = 0;
	private int armor = 10;
	private float speed = 2f;
	private int agility = 0;
	private int attackSpeed = 0;
	private int bonusSpeed = 0;
	private int strength = 0;
	private long timer = 0;
	private long bat = 1000; // base attack time = 1 second

	protected Vector2f offset = new Vector2f();
	private boolean isMoving;
	private boolean isDead = false;
	private LinkedList<Segment> path = new LinkedList<Segment>();
	private LinkedList<Segment> newPath;
	protected Vector2f move = new Vector2f();
	private int toX;
	private int toY;
	private Segment newDestination;
	private Map map;
	private boolean isAttacking = false;
	private ArrayList<Animation> animations = new ArrayList<>();

	public Unit(GameObjectType type, Map map) {
		super(type);
		this.map = map;
		timer = System.currentTimeMillis();
	}

	public void update() {
		if (isMoving && !isAttacking) {
			move();
		}

		for (Animation animation : animations) {
			animation.update();
		}

		regenerateHealth();

	}

	protected void regenerateHealth() {
		if (System.currentTimeMillis() - timer > 1000) {
			heal(regenPerSec);
			timer = System.currentTimeMillis();
		}

	}

	//posunie hr��a o posun, ktor� sa nastavil v calculateMovement()
	protected void move() {
		this.offset.x += move.x;
		this.offset.y += move.y;
		
		//ak u� jednotka bude na cie�ovom segmente alebo ho mierne prejde tak vycentruje jednotku a polo�� ju na segment
		if (Math.abs(offset.x) >= Math.abs(toX) && Math.abs(offset.y) >= Math.abs(toY)) {
			setMoving(false);
			if (move.x != 0) {
				move.x = move.x - (Math.abs(offset.x) - Math.abs(toX)) * move.x / Math.abs(move.x);
			}
			if (move.y != 0) {
				move.y = move.y - (Math.abs(offset.y) - Math.abs(toY)) * move.y / Math.abs(move.y);
			}
			setPosition();
			landOnSegment();
			offset.x = 0;
			offset.y = 0;
			return;
		}

		//nastavenie poz�cie
		setPosition();

	}

	protected void setPosition() {

	}

	//vypo��tanie ktor�m smerom sa bude hr�� h�ba� a nastavenie posunu
	private void calculateMovement() {

		toX = aSegment.getPoint().x - path.getFirst().getPoint().x;
		toY = aSegment.getPoint().y - path.getFirst().getPoint().y;

		if (toX > 0) {
			this.move.x = speed;
		} else if (toX < 0) {
			this.move.x = -speed;
		}

		if (toY > 0) {
			this.move.y = speed;
		} else if (toY < 0)
			this.move.y = -speed;

		this.setAnimationDirection();

		if (toX == 0 || toY == 0) {
			return;
		}

		if (Math.abs(toX / toY) == 2) {
			this.move.y /= 2;
		} else if (Math.abs(toY / toX) == 2) {
			this.move.x /= 2;
		}

	}

	//nastavenie anim�cie pod�a smeru pohybu
	private void setAnimationDirection() {
		if (animations.size() == 0)
			return;

		if (toX == 0) {
			if (toY < 0)
				for (Animation animation : animations) {
					animation.setDirection(Direction.SOUTH);
				}
			else {
				for (Animation animation : animations) {
					animation.setDirection(Direction.NORTH);
				}
			}
		} else if (toY == 0) {
			if (toX > 0) {
				for (Animation animation : animations) {
					animation.setDirection(Direction.WEST);
				}
			} else {
				for (Animation animation : animations) {
					animation.setDirection(Direction.EAST);
				}
			}
		} else if (toX > 0) {
			if (toY > 0) {
				for (Animation animation : animations) {
					animation.setDirection(Direction.NORTH_WEST);
				}
			} else {
				for (Animation animation : animations) {
					animation.setDirection(Direction.SOUTH_WEST);
				}
			}
		} else {
			if (toY > 0) {
				for (Animation animation : animations) {
					animation.setDirection(Direction.NORTH_EAST);
				}
			} else {
				for (Animation animation : animations) {
					animation.setDirection(Direction.SOUTH_EAST);
				}
			}
		}

	}

	//polo�enie jednotky na segment a prehodnotenie �i sa e�te jednotka dalej h�be alebo u� nie
	protected void landOnSegment() {

		aSegment.dispose();
		aSegment = path.getFirst();
		aSegment.setPlayer(this);
		path.removeFirst();
		move.x = 0;
		move.y = 0;

		if (newPath != null) {
			pathInterupted();
		} else if (!path.isEmpty()) {
			calculateMovement();
			setMoving(true);
		} else {
			pathDone();
			return;
		}

		if (path == null) {
			setMoving(false);
			pathDone();
			return;
		}

		if (!path.isEmpty()) {
			if (path.getFirst().isBlocked()) {
				this.path = PathFinder.calculatePath(this.aSegment, path.getLast());
				if (path == null) {
					setMoving(false);
					pathDone();
					return;
				}
				calculateMovement();
				setMoving(true);

			}
		}

		path.getFirst().setBlocked(true);

	}

	//cesta bola preru�en�
	protected void pathInterupted() {
		this.newPath = PathFinder.calculatePath(aSegment, newDestination);
		path = newPath;
		if (path == null) {
			setMoving(false);
			pathDone();
			return;
		}
		newPath = null;
		calculateMovement();
		setMoving(true);
	}

	//cesta skon�ila
	protected void pathDone() {
		if (animations.size() != 0) {
			for (Animation animation : animations) {
				animation.setAction(AnimationAction.IDLE);
			}
		}
	}

	public void takeDamage(int damage) {
		if (armor > 0)
			setHealth(health - damage + damage * getArmor() / 100);
		else
			setHealth(health - damage);

		if (health < 1) {
			health = 0;
			this.die();
		}
	}

	public void heal(int healing) {
		this.setHealth(health + healing);
		if (health > maxHealth) {
			health = maxHealth;
		}
	}

	protected void setHealth(int health) {
		this.health = health;

	}

	protected void die() {
		setDead(true);
	}

	public void move(Segment segment) {
		path = PathFinder.calculatePath(this.aSegment, segment);
		setMoving(true);
	}

	public boolean isMoving() {
		return isMoving;
	}

	public void setMoving(boolean isMoving) {
		this.isMoving = isMoving;
		if (isMoving) {
			if (animations.size() != 0) {
				for (Animation animation : animations) {
					animation.setAction(AnimationAction.WALK);
				}
			}
		}
	}

	public LinkedList<Segment> getPath() {
		return path;
	}

	public void setPath(LinkedList<Segment> path) {
		this.path = path;
	}

	// nastavenie cesty pre jednotku
	public void setPath(Segment segment) {

		if (path == null)
			path = new LinkedList<>();

		//check �i je segment blokovan�
		if (segment.isBlocked()) {
			//ak je blokovan� segment jednotka tak sa n�jde najbli��� vo�n� inak sa neh�ad� ni�
			if (segment.getGameObject() instanceof Unit && this != segment.getGameObject()) {
				segment = findCloseSegment(segment);
			} else
				return;

		}

		if (path.size() == 0) {
			this.path = PathFinder.calculatePath(this.aSegment, segment);
			if (path == null) {
				return;
			}
			if (path.size() > 0) {
				calculateMovement();
				setMoving(true);
				path.getFirst().setBlocked(true);

			}
		} else {

			this.newPath = PathFinder.calculatePath(path.getFirst(), segment);
			if (newPath == null) {
				return;
			}
			this.newDestination = segment;
			if (newPath.size() == 0) {
				newPath = null;
			}
		}
	}

	//hladanie najblizsieho segmentu ku segmentu z parametra
	private Segment findCloseSegment(Segment segment) {
		
		Segment ret;
		for (int x = -1; x < 2; x++) {
			for (int y = -1; y < 2; y++) {
				ret = map.getSegmentByMapPoint(new Point(segment.getXIndex() + x, segment.getYIndex() + y));
				
				if (ret != null && !ret.isBlocked()) {
					if (PathFinder.calculatePath(aSegment, ret)!=null) {
						return ret;
					}
				}
			}
		}
		return null;

	}

	
	public void addAnimation(AnimationType type) {
		Animation animation = new Animation(this, type);
		if (animations.size() != 0) {
			animation.setAction(this.animations.get(0).getAction());
			animation.setDirection(this.animations.get(0).getDirection());
		}
		this.animations.add(animation);

	}

	public void removeAnimation(AnimationType type) {
		for (int i = 0; i < this.animations.size(); i++) {
			if (animations.get(i).getType() == type) {
				animations.remove(i);
				return;
			}
		}
	}

	public ArrayList<Animation> getAnimation() {
		return animations;
	}

	public int getMaxHealth() {
		return maxHealth;
	}

	public void setMaxHealth(int maxHealth) {
		if (this.health == this.maxHealth)
			this.health = maxHealth;
		this.maxHealth = maxHealth;
		if (this.health > this.maxHealth)
			this.health = maxHealth;
	}

	public int getDamage() {
		return (int) (damage + ((double) damage / 100) * strength);
	}

	public int getArmor() {
		return (int) (armor + ((double) armor / 100) * agility);
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}

	public int getHealth() {
		return health;
	}

	public boolean isDead() {
		return isDead;
	}

	public void setDead(boolean isDead) {
		this.isDead = isDead;
	}

	public static Direction calculateDirection(Segment ourSegment, Segment otherSegment) {
		Direction ret = null;

		if (ourSegment.getXIndex() - otherSegment.getXIndex() == 0) {
			if (ourSegment.getYIndex() - otherSegment.getYIndex() > 0) {
				ret = Direction.NORTH_EAST;
			} else if (ourSegment.getYIndex() - otherSegment.getYIndex() < 0) {
				ret = Direction.SOUTH_WEST;
			}
		} else if (ourSegment.getYIndex() - otherSegment.getYIndex() == 0) {
			if (ourSegment.getXIndex() - otherSegment.getXIndex() > 0) {
				ret = Direction.NORTH_WEST;
			} else if (ourSegment.getXIndex() - otherSegment.getXIndex() < 0) {
				ret = Direction.SOUTH_EAST;
			}
		} else if (ourSegment.getYIndex() - otherSegment.getYIndex() == ourSegment.getXIndex()
				- otherSegment.getXIndex()) {
			if (ourSegment.getXIndex() - otherSegment.getXIndex() > 0) {
				ret = Direction.NORTH;
			} else {
				ret = Direction.SOUTH;
			}
		} else {
			if (ourSegment.getYIndex() - otherSegment.getYIndex() > 0) {
				ret = Direction.EAST;
			} else {
				ret = Direction.WEST;
			}
		}

		return ret;
	}

	public boolean isAttacking() {
		return isAttacking;
	}

	public void setAttacking(boolean isAttacking) {
		this.isAttacking = isAttacking;
	}

	public Vector2f getOffset() {
		return offset;
	}

	public void emptyPath() {
		path.clear();
	}

	public void attackDone() {

	}

	public void setSpeed(float speed) {
		this.speed = speed;
	}

	public float getSpeed() {
		return this.speed;
	}

	public void addAgility() {
		agility++;
		setAttackSpeed(attackSpeed);
		setBat(bat);
	}

	public void addStrength() {
		strength++;
		setMaxHealth(maxHealth + 1);
	}

	public void addInteligence() {
		inteligence++;
	}

	public void addVitality() {
		setMaxHealth(maxHealth + 3);
		vitality++;
	}

	public void setArmor(int armor) {
		this.armor = armor;
	}

	public void setAttackSpeed(int attackSpeed) {
		if (attackSpeed != 0) {
			attackSpeed += agility;
		}
		this.attackSpeed = attackSpeed;
	}

	public int getAttackSpeed() {
		return attackSpeed;
	}

	public int getAgility() {
		return agility;
	}

	public void setAgility(int agility) {
		setAttackSpeed(attackSpeed);
		this.agility = agility;
		setBat(bat);
	}

	public int getStrength() {
		return strength;
	}

	public void setStrength(int strength) {
		this.strength = strength;
	}

	public long getBat() {
		return bat;
	}

	public void setBat(long bat) {
		this.bat = bat;
		if (animations.size() != 0) {
			for (Animation animation : animations) {
				animation.setBAT(bat);
			}
		}

	}

	public int getInteligence() {
		return inteligence;
	}

	public void setInteligence(int inteligence) {
		this.inteligence = inteligence;
	}

	public int getVitality() {
		return vitality;
	}

	public int getBonusSpeed() {

		return bonusSpeed;
	}

	public void setBonusSpeed(int bonusSpeed) {
		this.bonusSpeed = bonusSpeed;
	}

	public void setRegen(int i) {
		regenPerSec = i;
	}

	public int getRegen() {
		return regenPerSec;
	}

	public int getBaseDamage() {
		return damage;
	}

	public Map getMap() {
		return map;
	}

}
