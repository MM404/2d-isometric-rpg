package com.matusmahut.isometricRPG.gameObject.units;

import com.matusmahut.isometricRPG.engine.Camera;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.Interactable;
import com.matusmahut.isometricRPG.graphics.Shader;
import com.matusmahut.isometricRPG.map.Map;
import com.matusmahut.isometricRPG.math.Matrix4f;
import com.matusmahut.isometricRPG.math.Vector2f;
import com.matusmahut.isometricRPG.math.Vector3f;

public abstract class NPC extends Unit implements Interactable {

	private Map map;
	private Vector2f position = new Vector2f();
	
	public NPC(GameObjectType type, Map map) {
		super(type, map);
		this.map=map;
	}
	
	public Map getMap() {
		return map;
	}	
	
	public void render(){
		Shader.STATIC.setUniformMat4f("ml_matrix", Matrix4f.translate(new Vector3f(Camera.getPosition().x - position.x, Camera.getPosition().y - position.y, 0)));
		super.render();
		Shader.STATIC.setUniformMat4f("ml_matrix", Matrix4f.translate(new Vector3f(Camera.getPosition().x, Camera.getPosition().y, 0)));
	}
	
	public void setPosition(){
		position.x+=move.x;
		position.y+=move.y;
	}

}
