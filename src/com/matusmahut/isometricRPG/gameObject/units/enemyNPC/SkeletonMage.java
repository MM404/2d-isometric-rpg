package com.matusmahut.isometricRPG.gameObject.units.enemyNPC;

import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.map.Map;

public class SkeletonMage extends EnemyNPC {

	public SkeletonMage(Map map) {
		super(GameObjectType.MINOTAUR, map);
		addAnimation(AnimationType.SKELETON_MAGE);
		setMaxHealth(120);
		setHealth(120);
		setDamage(8);
		setArmor(5);
		setBat(650);
		setXP(400);
	}

}
