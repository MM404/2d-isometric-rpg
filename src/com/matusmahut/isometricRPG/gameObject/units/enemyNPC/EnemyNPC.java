package com.matusmahut.isometricRPG.gameObject.units.enemyNPC;

import java.util.ArrayList;
import java.util.LinkedList;

import com.matusmahut.isometricRPG.animation.Animation;
import com.matusmahut.isometricRPG.animation.AnimationAction;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.units.NPC;
import com.matusmahut.isometricRPG.gameObject.units.Player;
import com.matusmahut.isometricRPG.gui.LifeBar;
import com.matusmahut.isometricRPG.map.Map;
import com.matusmahut.isometricRPG.map.Segment;

public abstract class EnemyNPC extends NPC {

	private Player player;
	private boolean attackMode;
	private LifeBar lb;
	private int xp = 320;

	public EnemyNPC(GameObjectType type, Map map) {
		super(type, map);
		player = map.getPlayer();	
	}

	public void setLifeBar(){
		lb= new LifeBar(this, aSegment.getPoint().x+25, aSegment.getPoint().y-80, 50);
		
	}
	
	
	public void move(){
		super.move();
		lb.setPosition(aSegment.getPoint().x+25-this.getOffset().x, aSegment.getPoint().y-this.offset.y-80, 50);
	}
	
	public void render(){		
		super.render();
	}
	
	public void renderLifeBar(){
	
		lb.render();
	}
	
	public void update() {
		//pozer� �i nie je na bl�zku hr�� ak �no tak za�to��
		if (!isAttacking()&& !attackMode) {
		Segment playerSegment = player.getaSegment();
		if (Math.abs(playerSegment.getXIndex() - aSegment.getXIndex()) < 3
				&& Math.abs(playerSegment.getYIndex() - aSegment.getYIndex()) < 3) {
			attackMode=true;
			attack(playerSegment);
			
		} 
		}
		super.update();
	}
	
	protected void die(){
		player.addXP(xp);
		super.die();
	}
	
	protected void landOnSegment(){
		
		super.landOnSegment();
		if (attackMode && !isAttacking()) {
			attack(player.getaSegment());		
		}
		
		
	}
	
	public void takeDamage(int damage){
		super.takeDamage(damage);
		lb.setHealth(getHealth());
	}

	//ak je hr�� bl�zko prevedie �tok ak nie tak nastav� cestu k hr��ovi
	private void attack(Segment playerSegment) {
		if (Math.abs(playerSegment.getXIndex() - aSegment.getXIndex()) < 2
				&& Math.abs(playerSegment.getYIndex() - aSegment.getYIndex()) < 2) {
			ArrayList<Animation> animations=getAnimation();
			if (animations.get(0).getAction() != AnimationAction.ATTACK) {
				for (Animation animation : animations) {
					animation.setAction(AnimationAction.ATTACK);
					animation.setDirection(calculateDirection(aSegment, playerSegment));
				}
				setAttacking(true);				
				setMoving(false);
				
				LinkedList<Segment> path = getPath();
				if (path!=null && path.size()>0) {
					path.getFirst().setBlocked(false);
					path.clear();
				}
				
				
			}

		} else{
			setPath(playerSegment);
			
		}
		
	}

	public void attackDone() {
		player.takeDamage(getDamage());
		setAttacking(false);
		attackMode=false;
	}

	@Override
	public void interact(Player player) {
		player.attack(this);
	}
	
	public void setXP(int xp){
		this.xp = xp;
	}

}
