package com.matusmahut.isometricRPG.gameObject.units.enemyNPC;

import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.map.Map;

public class Skeleton extends EnemyNPC {

	public Skeleton(Map map) {
		super(GameObjectType.MINOTAUR, map);
		addAnimation(AnimationType.SKELETON);
		setMaxHealth(100);
		setHealth(100);
		setDamage(4);
		setArmor(15);
		setBat(500);	
		setXP(350);
	}
	

}
