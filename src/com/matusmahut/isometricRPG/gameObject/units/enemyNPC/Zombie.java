package com.matusmahut.isometricRPG.gameObject.units.enemyNPC;


import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.map.Map;

public class Zombie extends EnemyNPC {

	public Zombie(Map map) {
		super(GameObjectType.ZOMBIE, map);
		addAnimation(AnimationType.ZOMBIE);
		setMaxHealth(250);
		setHealth(250);
		setDamage(15);
		setArmor(0);
		setBat(1300);
		setXP(700);
	}

}
