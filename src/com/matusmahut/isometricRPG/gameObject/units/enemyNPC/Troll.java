package com.matusmahut.isometricRPG.gameObject.units.enemyNPC;

import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.map.Map;

public class Troll extends EnemyNPC {

	public Troll(Map map) {
		super(GameObjectType.MINOTAUR, map);
		addAnimation(AnimationType.SKELETON_MAGE);
		setMaxHealth(500);
		setHealth(500);
		setDamage(15);
		setArmor(0);
		setBat(1000);
		setXP(800);
	}

}
