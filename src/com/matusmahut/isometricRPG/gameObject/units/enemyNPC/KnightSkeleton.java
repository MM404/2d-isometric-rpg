package com.matusmahut.isometricRPG.gameObject.units.enemyNPC;

import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.map.Map;

public class KnightSkeleton extends EnemyNPC {

	public KnightSkeleton(Map map) {
		super(GameObjectType.MINOTAUR, map);
		addAnimation(AnimationType.SKELETON_KNIGHT);
		setMaxHealth(100);
		setHealth(100);
		setDamage(13);
		setArmor(40);
		setBat(500);	
		setXP(800);
	}
	

}
