package com.matusmahut.isometricRPG.gameObject.units.enemyNPC;

import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.map.Map;

public class Minotaur extends EnemyNPC {

	public Minotaur(Map map) {
		super(GameObjectType.MINOTAUR, map);
		addAnimation(AnimationType.MINOTAUR);
		setMaxHealth(250);
		setHealth(250);
		setDamage(5);
		setArmor(10);
		setBat(900);	
		setXP(900);
	}
	

}
