package com.matusmahut.isometricRPG.gameObject.units.enemyNPC;

import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.map.Map;

public class Bandit extends EnemyNPC {

	public Bandit(Map map) {
		super(GameObjectType.HERO, map);
		addAnimation(AnimationType.BANDIT);
		setMaxHealth(100);
		setHealth(100);
		setDamage(6);
		setArmor(4);
		setBat(600);	
		setXP(400);
	}

}
