package com.matusmahut.isometricRPG.gameObject.units;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import com.matusmahut.isometricRPG.animation.Animation;
import com.matusmahut.isometricRPG.animation.AnimationAction;
import com.matusmahut.isometricRPG.animation.AnimationType;
import com.matusmahut.isometricRPG.engine.Camera;
import com.matusmahut.isometricRPG.engine.state.StateManager;
import com.matusmahut.isometricRPG.gameObject.GameObject;
import com.matusmahut.isometricRPG.gameObject.GameObjectType;
import com.matusmahut.isometricRPG.gameObject.LootObject;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.InventoryObject;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.buff.AbstractBuff;
import com.matusmahut.isometricRPG.gameObject.inventoryObject.wearableObject.*;
import com.matusmahut.isometricRPG.gameObject.units.enemyNPC.EnemyNPC;
import com.matusmahut.isometricRPG.gameObject.units.friendlyNPC.FriendlyNPC;
import com.matusmahut.isometricRPG.graphics.Texture;
import com.matusmahut.isometricRPG.gui.HUD;
import com.matusmahut.isometricRPG.gui.Inventory;
import com.matusmahut.isometricRPG.gui.LootMenu;
import com.matusmahut.isometricRPG.map.Map;
import com.matusmahut.isometricRPG.map.Segment;
import com.matusmahut.isometricRPG.quest.ConversationType;
import com.matusmahut.isometricRPG.quest.Quest;
import com.matusmahut.isometricRPG.quest.QuestType;

public class Player extends Unit {

	public enum AnimationPart {
		BODY, HEAD, WEAPON, SHIELD, ALL;
	}

	//zamknute objekty ku ktorym hrac prave ide a chce interagovat
	private LootObject loot;
	private Unit lockedNPC;
	
	private boolean isLooting;
	private int level=1;
	private int xp = 0;
	private int xpBonus = 0;
	private int toxicity = 0;
	private int points = 30;
	private HUD hud;
	private final Fists fists = new Fists();
	private final Clothes clothes = new Clothes();
	private LootMenu lootMenu;
	private Inventory inventory;
	private ArrayList<AbstractBuff> buffs;
	private Segment attackedSegment;
	private HashMap<AnimationPart, GameObject> playerParts;
	private Armor armor = clothes;
	private Weapon weapon = fists;
	private Segment origin;
	private Quest quest;
	private ArrayList<Quest> quests;
	private StateManager stateManager;

	public Player(GameObjectType type, Map map) {
		super(type, map);
		buffs = new ArrayList<AbstractBuff>();
		this.setDamage(0);
		playerParts = new HashMap<>();
		setSpeed(4f);
		weapon.use(this);
		this.setRegen(1);
		quests = new ArrayList<>();
	}

	//hr�� pou��va viacero anim�cii(hlava, brnenie, mec atd)
	public void initAnimations() {

		origin = aSegment;
		GameObject gObject = new GameObject(GameObjectType.PLAYER);
		armor.use(this);
		gObject.setPosition(aSegment);
		playerParts.put(AnimationPart.HEAD, gObject);
		addAnimation(AnimationType.HERO_HEAD3);

	}

	public void update() {
		super.update();
		ArrayList<AbstractBuff> endedBuffs = new ArrayList<AbstractBuff>();
		for (AbstractBuff buff : buffs) {
			buff.update();
			if (buff.ended()) {
				endedBuffs.add(buff);
			}
		}
		buffs.removeAll(endedBuffs);
	}

	public void render() {
		//kreslenie aktivnych textur vestkych casti hraca(hlava, brnenie, mec atd)
		for (GameObject gObject : playerParts.values()) {
			gObject.render();
		}
	}

	public void pathDone() {
		super.pathDone();
		if (loot != null) {
			loot(loot);
		} 
	}

	protected void landOnSegment() {

		/*
		 * if (lockedNPC != null) { if (lockedNPC instanceof FriendlyNPC) {
		 * interact((FriendlyNPC) lockedNPC);
		 * 
		 * } else if (!isAttacking()) attack((EnemyNPC) lockedNPC); }
		 */
		super.landOnSegment();
		if (lockedNPC != null) {

			if (!isAttacking()) {

				if (lockedNPC instanceof EnemyNPC) {
					attack((EnemyNPC) lockedNPC);
				} 
			}
		}
		quest.handlePlayerPositionChange(aSegment);
	}

	public void attackDone() {
		GameObject gObj = attackedSegment.getGameObject();
		if (gObj instanceof Unit) {
			((Unit) gObj).takeDamage(getDamage());
		}
		setAttacking(false);

	}

	public void heal(int healing) {
		super.heal(healing);
		hud.setHealth(this.health);
	}

	public void takeDamage(int damage) {
		super.takeDamage(damage);
		hud.setHealth(this.health);
	}

	public void pathInterupted() {
		super.pathInterupted();
		if (loot == null) {
			return;
		}
		if (getPath().getLast() != loot.getLootSegment()) {
			loot = null;
		}
	}

	protected void setPosition() {
		Camera.move2f(move.x, move.y);
	}
	
	public void setGameStateManager(StateManager manager){
		this.stateManager=manager;
	}

	public void setPath(Segment lootSegment, LootObject lootObject) {

		super.setPath(lootSegment);
		loot = lootObject;
	}

	public void loot(LootObject lootObject) {
		if (aSegment==lootObject.getLootSegment()) {
		isLooting = true;
		lootMenu.setLoot(lootObject);
		loot = null;
		}
	}

	public void addToInventory(InventoryObject object) {
		inventory.addObject(object);
	}

	public LootObject getLoot() {

		return loot;
	}

	public void setLoot(LootObject loot) {
		this.loot = loot;
	}

	public boolean isLooting() {
		return isLooting;
	}

	public void setLooting(boolean isLooting) {
		this.isLooting = isLooting;
	}

	public void lootMenu(HUD hud) {
		this.hud = hud;
	}

	public void setHud(HUD hud) {
		this.hud = hud;
	}
	
	//pridanie questu
	public void setQuest(QuestType type){
		quest = new Quest(this, type);
		quest.setQuestStateDescription();
		quests.add(quest);
	}

	public int getToxicity() {
		return toxicity;
	}

	public void setLootMenu(LootMenu lootMenu) {
		this.lootMenu = lootMenu;
	}

	public void setInventory(Inventory inventory) {
		this.inventory = inventory;

	}

	public void use(InventoryObject iObject) {
		iObject.use(this);

	}

	public void addBuff(AbstractBuff buf) {
		buf.start();
		buffs.add(buf);
	}

	public void removeBuff(AbstractBuff buf) {
		buffs.remove(buf);

	}

	public void setDamage(int damage) {
		super.setDamage(damage);
		if (hud != null) {
			hud.setDamage(getDamage());
		}

	}

	public void setPath(Segment s) {
		if (isAttacking()) {
			return;
		}
		super.setPath(s);
	}

	//ak je npc bl�zko interaguje, ak nie pr�de k nemu
	public void interact(FriendlyNPC npc) {
		Segment npcSegment = npc.getaSegment();
		lockedNPC = npc;
		if (Math.abs(npcSegment.getXIndex() - aSegment.getXIndex()) < 2
				&& Math.abs(npcSegment.getYIndex() - aSegment.getYIndex()) < 2) {
			lockedNPC = null;
			quest.handleDialogInteraction(npc);

		} else {
			setPath(npcSegment);
		}

	}

	//ak je npc bl�zko prevedie �tok, ak nie pr�de k nemu
	public void attack(EnemyNPC enemyNPC) {
		Segment enemySegment = enemyNPC.getaSegment();
		lockedNPC = enemyNPC;
		if (Math.abs(enemySegment.getXIndex() - aSegment.getXIndex()) < 2
				&& Math.abs(enemySegment.getYIndex() - aSegment.getYIndex()) < 2) {

			ArrayList<Animation> animations = getAnimation();
			if (animations.get(0).getAction() != AnimationAction.ATTACK) {
				for (Animation animation : animations) {
					animation.setAction(AnimationAction.ATTACK);
					animation.setDirection(calculateDirection(aSegment, enemySegment));
				}
				lockedNPC = null;
				setAttacking(true);
				attackedSegment = enemySegment;
				setMoving(false);
				LinkedList<Segment> path = getPath();
				if (path != null && path.size() > 0) {
					path.getFirst().setBlocked(false);
					path.clear();
				}

			}
		} else {

			setPath(enemySegment);
		}

	}

	//vr�ti npc, s ktor�m hr�� pr�ve nie�o rob�(ide k nemu)
	public Unit getLockedNPC() {
		return lockedNPC;
	}

	@Override
	public void addAnimation(AnimationType type) {
		GameObject gObject = new GameObject(GameObjectType.PLAYER);
		gObject.setPosition(origin);
		playerParts.put(type.getPart(), gObject);
		super.addAnimation(type);
	}

	public void removeAnimation(AnimationType type) {
		playerParts.remove(type.getPart());
		super.removeAnimation(type);
	}

	public void setLockedNPC(Unit lockedNPC) {
		this.lockedNPC = lockedNPC;
	}

	public void setTexture(Texture texture, AnimationPart type) {

		playerParts.get(type).setTexture(texture);
	}

	public void setWeapon(Weapon weapon) {
		this.weapon.highLight();
		this.weapon.use(this);
		
		
		if (weapon==this.weapon) {
			this.weapon=fists;
		} else
		this.weapon = weapon;	
		this.weapon.highLight();
		this.weapon.use(this);
		
	}

	public void setArmor(Armor armor) {
		this.armor.use(this);
		this.armor.highLight();
		
		if (armor==this.armor) {
			this.armor=clothes;
		} else
		this.armor = armor;	
		
		this.armor.use(this);
		this.armor.highLight();
	}
	
	/*public void setRing(Ring ring) {
		this.ring.use(this);
		this.ring.highLight();
		
		if (ring==this.ring) {
			this.ring=null;
			return;
		} else
		this.ring = ring;	
		
		this.ring.use(this);
		this.ring.highLight();
	}*/
	
	/*
	public void setShield(Shield shield) {
		this.shield.use(this);
		this.shield.highLight();
		
		if (shield==this.shield) {
			this.shield=null;
			return;
		} else
		this.shield = armor;	
		
		this.armor.use(this);
		this.armor.highLight();
	} */

	public void wear(InventoryObject iObject) {
		if (iObject instanceof Weapon) {
			setWeapon((Weapon)iObject);
		} else if (iObject instanceof Armor) {
			setArmor((Armor)iObject);
		}

	}

	public int getLevel() {
		return this.level;
	}
	
	public void addXP(int xp) {
		if (xpBonus!=0) {
			xp+=((double)xp/100)*xpBonus;
		}
		this.xp+=xp;
		if (this.xp>=1000) {
			levelUp();
		}
	}

	private void levelUp() {
		level++;
		xp-=1000;
		points+=10;
		if (xp>=1000) {
			levelUp();
		}
	}
	
	public void takePoints(int i){
		points-=i;
	}
	
	public int getPoints(){
		return points;
	}
	
	public void addInteligence(){
		xpBonus+=2;
		super.addInteligence();
	}
	
	public void addAgility(){
		
		super.addAgility();
		hud.setArmor(getArmor());
	}
	
	public void addStrength(){
		
		super.addStrength();
		hud.setDamage(getDamage());
	}
	
	public void setArmor(int armor){
		super.setArmor(armor);
		if (hud==null) 
			return;
		hud.setArmor(getArmor());
	}
	
	public void setMaxHealth(int maxHealth){
		super.setMaxHealth(maxHealth);
		hud.setMaxHeath(getMaxHealth());
		hud.setHealth(getHealth());
	}
	
	public HUD getHud(){
		return hud;
	}

	public void setConversation(ConversationType type, NPC npc) {
		stateManager.setActiveState(type, getMap(), npc);
		
	}

	public Quest getQuest() {
		
		return quest;
	}

}
