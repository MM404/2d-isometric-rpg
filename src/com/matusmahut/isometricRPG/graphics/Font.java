package com.matusmahut.isometricRPG.graphics;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Map;

import com.matusmahut.isometricRPG.engine.Camera;
import com.matusmahut.isometricRPG.math.Vector3f;

import static org.lwjgl.opengl.GL11.*;

public class Font {

	
	private final Map<Integer, String> CHARS = new HashMap<Integer, String>() {
		{
			put(0, "ABCDEFGHIJKLMNOPQRSTUVWXYZ");
			put(1, "abcdefghijklmnopqrstuvwxyz");
			put(2, "0123456789");
			put(3, "�������");
			put(4, " $+-*/=%\"'#@&_(),.;:?!\\|<>[]�`^~");
		}
	};

	
	private java.awt.Font font;
	private FontMetrics fontMetrics;
	private BufferedImage bufferedImage;
	private int fontTextureId;
	
	public enum FontType{
		
		SERIF("resources/fonts/serif/serif.ttf"),
		ROBOTO_BLACK("resources/fonts/roboto/Roboto-Black.ttf");
		
		private final String path;
		
		FontType(String path){
			this.path=path;
		}
		
		public String getPath(){
			return path;
		}
		
	}

	public float getFontImageWidth() {
		return (float) CHARS.values().stream().mapToDouble(e -> fontMetrics.getStringBounds(e, null).getWidth()).max()
				.getAsDouble();
	}

	public float getFontImageHeight() {
		return (float) CHARS.keySet().size() * (this.getCharHeight());
	}

	public float getCharX(char c) {
		String originStr = CHARS.values().stream().filter(e -> e.contains("" + c)).findFirst().orElse("" + c);
		return (float) fontMetrics.getStringBounds(originStr.substring(0, originStr.indexOf(c)), null).getWidth();
	}

	public float getCharY(char c) {
		float lineId = (float) CHARS.keySet().stream().filter(i -> CHARS.get(i).contains("" + c)).findFirst().orElse(0);
		return this.getCharHeight() * lineId;
	}

	public float getCharWidth(char c) {
		return fontMetrics.charWidth(c);
	}

	public float getCharHeight() {
		return (float) (fontMetrics.getMaxAscent() + fontMetrics.getMaxDescent());
	}

	public Font(String path, float size) throws Exception {
		this.font = java.awt.Font.createFont(java.awt.Font.TRUETYPE_FONT, new File(path)).deriveFont(size);

		// generovanie bufferedImage
		GraphicsConfiguration gc = GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice()
				.getDefaultConfiguration();
		Graphics2D graphics = gc.createCompatibleImage(1, 1, Transparency.TRANSLUCENT).createGraphics();
		graphics.setFont(font);

		fontMetrics = graphics.getFontMetrics();
		bufferedImage = graphics.getDeviceConfiguration().createCompatibleImage((int) getFontImageWidth(),
				(int) getFontImageHeight(), Transparency.TRANSLUCENT);

		// generovanie text�ry
		fontTextureId = glGenTextures();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, fontTextureId);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (int) getFontImageWidth(), (int) getFontImageHeight(), 0, GL_RGBA,
				GL_UNSIGNED_BYTE, asByteBuffer());

		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	}

	// kreslenie textu
	public void drawText(String text, int x, int y) {
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, this.fontTextureId);		
		glBegin(GL_QUADS);

		int xTmp = x;
		for (char c : text.toCharArray()) {
			float width = getCharWidth(c);
			float height = getCharHeight();
			float cw = 1f / getFontImageWidth() * width;
			float ch = 1f / getFontImageHeight() * height;
			float cx = 1f / getFontImageWidth() * getCharX(c);
			float cy = 1f / getFontImageHeight() * getCharY(c);

			glTexCoord2f(cx, cy);
			glVertex3f(xTmp, y, 0);

			glTexCoord2f(cx + cw, cy);
			glVertex3f(xTmp + width, y, 0);

			glTexCoord2f(cx + cw, cy + ch);
			glVertex3f(xTmp + width, y + height, 0);

			glTexCoord2f(cx, cy + ch);
			glVertex3f(xTmp, y + height, 0);

			xTmp += width;
		}

		glEnd();
		glDisable(GL_TEXTURE_2D);
	}
	
	//kreslenie textu relativne voci kamere
	public void drawTextRelative(String text, int x, int y) {
		Vector3f camera = Camera.getPosition();
		
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, this.fontTextureId);		
		glBegin(GL_QUADS);
		

		int xTmp = x - (int)camera.x;
		y-=(int)camera.y;
		for (char c : text.toCharArray()) {
			float width = getCharWidth(c);
			float height = getCharHeight();
			float cw = 1f / getFontImageWidth() * width;
			float ch = 1f / getFontImageHeight() * height;
			float cx = 1f / getFontImageWidth() * getCharX(c);
			float cy = 1f / getFontImageHeight() * getCharY(c);

			glTexCoord2f(cx, cy);
			glVertex3f(xTmp, y, 0);

			glTexCoord2f(cx + cw, cy);
			glVertex3f(xTmp + width, y, 0);

			glTexCoord2f(cx + cw, cy + ch);
			glVertex3f(xTmp + width, y + height, 0);

			glTexCoord2f(cx, cy + ch);
			glVertex3f(xTmp, y + height, 0);

			xTmp += width;
		}

		glEnd();
		glDisable(GL_TEXTURE_2D);
	}


	
	public ByteBuffer asByteBuffer() {

		ByteBuffer byteBuffer;

		// vykreslenie pismen na obrazok
		Graphics2D imageGraphics = (Graphics2D) bufferedImage.getGraphics();
		imageGraphics.setFont(font);
		imageGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_OFF);
		imageGraphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);

		
		imageGraphics.setColor(java.awt.Color.WHITE);
		CHARS.keySet().stream().forEach(i -> imageGraphics.drawString(CHARS.get(i), 0,
				fontMetrics.getMaxAscent() + (this.getCharHeight() * i)));

		// generovanie d�t text�ry
		int[] pixels = new int[bufferedImage.getWidth() * bufferedImage.getHeight()];
		bufferedImage.getRGB(0, 0, bufferedImage.getWidth(), bufferedImage.getHeight(), pixels, 0,
				bufferedImage.getWidth());
		byteBuffer = ByteBuffer.allocateDirect((bufferedImage.getWidth() * bufferedImage.getHeight() * 4));

		for (int y = 0; y < bufferedImage.getHeight(); y++) {
			for (int x = 0; x < bufferedImage.getWidth(); x++) {
				int pixel = pixels[y * bufferedImage.getWidth() + x];
				byteBuffer.put((byte) ((pixel >> 16) & 0xFF)); // R
				byteBuffer.put((byte) ((pixel >> 8) & 0xFF)); // G
				byteBuffer.put((byte) (pixel & 0xFF)); // B
				byteBuffer.put((byte) ((pixel >> 24) & 0xFF)); // A
			}
		}

		byteBuffer.flip();

		return byteBuffer;
	}
}