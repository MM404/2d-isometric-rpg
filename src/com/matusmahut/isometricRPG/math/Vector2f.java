package com.matusmahut.isometricRPG.math;

public class Vector2f {

	public float x;
	public float y;

	public Vector2f() {
		x = 0f;
		y = 0f;
	}

	public Vector2f(float paX, float paY) {
		x = paX;
		y = paY;
	}

}
