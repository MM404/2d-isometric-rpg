package com.matusmahut.isometricRPG.math;

public class Vector3f {

	public float x;
	public float y;
	public float z;

	public Vector3f() {
		x = 0f;
		y = 0f;
		z = 0f;
	}

	public Vector3f(float paX, float paY, float paZ) {
		x = paX;
		y = paY;
		z = paZ;
	}

	public float getX() {
		return x;
	}

	public float getY() {
		return y;
	}

	public float getZ() {
		return z;
	}

}
